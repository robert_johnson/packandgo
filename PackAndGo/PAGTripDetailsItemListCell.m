//
//  PAGBaggageCell.m
//  PackAndGo
//
//  Created by robert johnson on 3/5/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGTripDetailsItemListCell.h"
#import "DDLog.h"

@implementation PAGTripDetailsItemListCell
static const int ddLogLevel = LOG_LEVEL_OFF;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)baggageNameEdited:(id)sender {
    DDLogInfo(@"baggage name edited");
}

- (IBAction)baggageIsHoldEdited:(id)sender {
        DDLogInfo(@"baggage is hold edited");
}
@end
