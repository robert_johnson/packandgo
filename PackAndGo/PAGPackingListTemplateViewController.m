//
//  PAGPackingListTemplateViewController.m
//  PackAndGo
//
//  Created by robert johnson on 2/6/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGPackingListTemplateViewController.h"
#import "PAGDbOperations.h"
#import "PAGPackingListTemplate.h"
#import "DDLog.h"

static const int ddLogLevel = LOG_LEVEL_OFF;

@interface PAGPackingListTemplateViewController ()

@end

@implementation PAGPackingListTemplateViewController
@synthesize dateFormatter = _dateFormatter;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    DDLogInfo(@"packinglisttemplate view");
    DDLogInfo(@"***PackingListTemplate Calling database initialise");
    PAGDbOperations *db = [PAGDbOperations alloc];
    DDLogInfo(@"***PackingListTemplateDone Calling database initialise");
    [db initDb];
    [self setDb:db];
    //set up a date formatter
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"dataChanged" object:nil];
}
-(void) refresh {
    if (_db) {
        [self setItem:[[_db item] mutableCopy]];
        [_tableView reloadData];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

-(void) refresh {
    
    
}
#pragma mark -
#pragma mark templtaeview tbale dtatsource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    DDLogInfo(@"templateTableView %@ is asking for numberOfSectionsInCollectionView", tableView);
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DDLogInfo(@"templateTableView %@ is asking for numberOfItemsInSection %d", tableView, section);
    int count = [_db countPackingListTemplate];
    DDLogInfo(@"Got it - %d",count);
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"templateTableView %@ is asking for item at index %@", tableView, indexPath);
    PAGTemplateListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PAGTemplateCell" ];
    PAGPackingListTemplate *packingListTemplate = [_db getPackingListTemplateForIndex:indexPath ] ;
    cell.title.text = packingListTemplate.name;
    
    return cell;
}



- (void)viewDidUnload {
    [super viewDidUnload];
}
- (IBAction)doneButtonPressed:(id)sender {
    DDLogInfo(@"done button pressed");
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
