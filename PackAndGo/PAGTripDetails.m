//
//  PAGTripDetails.m
//  PackAndGo
//
//  Created by robert johnson on 2/21/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGTripDetails.h"

@implementation PAGTripDetails

#pragma mark -
#pragma mark data encode and decode functions
- (void) encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_tripName forKey:@"_tripName"];
    [aCoder encodeObject:_tripStartDate forKey:@"_tripStartDate"];
    
    [aCoder encodeObject:_packingListTemplate forKey:@"_packingListTemplate"];
    [aCoder encodeObject:_bags forKey:@"_bags"];
    [aCoder encodeObject:_items forKey:@"_items"];
    [aCoder encodeObject:_itemLists forKey:@"_itemLists"];

}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self=[super init]) {

        self.tripName = [aDecoder decodeObjectForKey:@"_tripName"];
        self.tripStartDate = [aDecoder decodeObjectForKey:@"_tripStartDate"];
        
        self.packingListTemplate = [aDecoder decodeObjectForKey:@"_packingListTemplate"];
        self.bags = [aDecoder decodeObjectForKey:@"_bags"];
        self.items = [aDecoder decodeObjectForKey:@"_items"];
        self.itemLists = [aDecoder decodeObjectForKey:@"_itemLists"];

    }
    return self;
}

@end
