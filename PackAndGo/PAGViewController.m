//
//  PAGViewController.m
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGDbOperations.h"
#import "PAGViewController.h"
#import "PAGTripListCell.h"
#import "PAGPackingList.h"
#import "PAGTripDetails.h"
#import "PAGTripDetailsViewController.h"
#import "PAGPackingItem.h"


#import "DDLog.h"
static const int ddLogLevel = LOG_LEVEL_OFF;



@interface PAGViewController () {
    NSMutableArray *tripDetails;
}

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@end


@implementation PAGViewController {

    ADBannerView *_bannerView;
    IBOutlet UIView *contentView;
    BOOL bannerIsVisible;
    
}


- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
        
    DDLogInfo(@"MainView : Calling database initialise");
    PAGDbOperations *db = [[PAGDbOperations alloc] init];
    [db initDb];
    [self setDb:db];
    // get a local copy of trip details;
    
    tripDetails = [[_db tripDetails] mutableCopy];
    //set up a date formatter
    _dateFormatter = [[NSDateFormatter alloc] init];
    //[_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [_dateFormatter setDateStyle:NSDateFormatterFullStyle];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadge) name:@"updateBadge" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"dataChanged" object:nil];

    // load the ad banner
    /**CGSize bannerSize;
    bannerSize = [ADBannerView sizeFromBannerContentSizeIdentifier:ADBannerContentSizeIdentifierPortrait];
    _bannerView = [[ADBannerView alloc] initWithFrame:CGRectMake(0, contentView.bounds.size.height, bannerSize.width, bannerSize.height)];
    _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    _bannerView.delegate = self;
    _bannerView.hidden = YES;
     **/
    _bannerView = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 0, 0)];
    _bannerView.delegate = self;
    _bannerView.hidden = YES;
    [self setBannerPortrait];
    //[self.view addSubview:_bannerView];

}

-(void)viewDidAppear:(BOOL)animated {
    DDLogInfo(@"View Did Appear");
    [self.view addSubview:_bannerView];
    [_bannerView setDelegate:self];
    [self sortTripDetails];
    [_PAGTripListTableView reloadData];
    DDLogInfo(@"h=%f",self.view.frame.size.height);
    DDLogInfo(@"w=%f",self.view.frame.size.width);
    DDLogInfo(@"x=%f",self.view.frame.origin.x);
    DDLogInfo(@"y=%f",self.view.frame.origin.y);

}
-(void)viewDidDisappear:(BOOL)animated {

    DDLogInfo(@"View Did Disappear");
        [_bannerView removeFromSuperview];
    [_bannerView setDelegate:nil];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) refresh {
    DDLogInfo(@"MainView : Notified that data has changed, so reload the table");
    if (_db ) {
        tripDetails = [[_db tripDetails] mutableCopy];
    }
    [self sortTripDetails];
    [_PAGTripListTableView reloadData];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [_db saveData];
}


#pragma mark -
#pragma mark PSUICollectionViewDatasource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = tripDetails.count;
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PAGTripListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PAGTripListCell" ];
    PAGTripDetails *tripDetail = [tripDetails objectAtIndex:indexPath.row] ;
    cell.tripName.text = tripDetail.tripName;
    cell.dateDisplayField.text = [_dateFormatter stringFromDate:tripDetail.tripStartDate];
    return cell;
}

#pragma mark segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"packingListToTemplateList"]) {
        DDLogInfo(@"MainView :  : PackingListToTemplateList segue");
    } else if ([segue.identifier isEqualToString:@"mainToTripDetails"]) {
        DDLogInfo(@"MainView : mainToTripDetails");
        UITabBarController *x = segue.destinationViewController;
        PAGTripDetailsViewController *t = (PAGTripDetailsViewController *)x.viewControllers[0];
        [t setTripId:nil];
        [t setDb:_db];
        
        [x setDelegate:t];
    }else if ([segue.identifier isEqualToString:@"mainToTripDetailsWithId"]) {
        DDLogInfo(@"MainView : mainToTripDetailsWithId");
        UITabBarController *x = segue.destinationViewController;
        PAGTripDetailsViewController *t = (PAGTripDetailsViewController *)x.viewControllers[0];
        NSNumber *row = [NSNumber numberWithInt:[_PAGTripListTableView indexPathForCell:(PAGTripListCell*)sender].row];
        [t setTripId:row];
        [t setDb:_db];
        [x setDelegate:t];
        
    }
}

- (void)viewDidUnload {
    [_db saveData];
    contentView = nil;
    [super viewDidUnload];
}

// sort my local copy of trip details
-(void) sortTripDetails{
    // now I must sort the array into date order..!!??
    // + 
    NSDate *today = [NSDate date];
    NSArray *sorted;
    sorted = [tripDetails sortedArrayUsingComparator:^(id obj1, id obj2) {
        NSDate *obj1Date = [(PAGTripDetails *)obj1 tripStartDate];
        NSDate *obj2Date = [(PAGTripDetails *)obj2 tripStartDate];
        NSComparisonResult result = [obj1Date compare:obj2Date ];
        return result;
    }];
    // now sort so that 'today' is the comparator
    NSMutableArray *objectsToMove = [[NSMutableArray alloc]init];
    for (PAGTripDetails *t in [sorted copy]) {
        //is t in the past?
        NSComparisonResult result = [t.tripStartDate compare:today ];
        if (result == NSOrderedAscending) {
            //  yes
            //note to move it to the bottom of the list
            [objectsToMove addObject:t];
        }
    }
    // copy the date ordered array
    if ([tripDetails isEqualToArray:sorted]) {
    } else {
        DDLogInfo(@"copy sorted array");
        // else make a copy and inform the table that it must reload its data
        tripDetails = [sorted mutableCopy];
        
    }
    // now really move them
    for (PAGTripDetails *t in objectsToMove) {
        //remove from where it is
        [tripDetails removeObject:t ];
        // sstick it on the end
        [tripDetails addObject:t];
    }
    
    // in the end, if we have more than ... 10?? trips, remove the end one
    if ([tripDetails count] >10) {
        [tripDetails removeLastObject];
    }
    
    
    [_db setTripDetails:tripDetails];
    [_db saveData];
    [_PAGTripListTableView reloadData];
    
    
    
}



-(void)updateBadge {
    
    // update badge with the top most trip's outstanding 'to pack' count
    PAGTripDetails *tripDetail = [tripDetails objectAtIndex:0];
    int count = 0;
    for (PAGPackingItem *i in [[tripDetail items] objectEnumerator]) {
        if (!i.packed) {
            count++;
        }
    }
    [UIApplication sharedApplication].applicationIconBadgeNumber = count;
    
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)io {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return YES;
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait(io)) {
            return YES;
        } else {
            return NO;
        }
    }
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        [self setBannerPortrait];
    } else {
        [self setBannerLandscape];

    }
}

#pragma mark -
#pragma mark stuff added for the ad banners

-(void ) setBannerLandscape {
    DDLogInfo(@"Banner to Landscape");
    _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    CGSize bannerSize = [ADBannerView sizeFromBannerContentSizeIdentifier:ADBannerContentSizeIdentifierLandscape];
    if (_bannerView.bannerLoaded == 0) {
        [_bannerView setFrame:CGRectMake(0, self.view.bounds.size.height, bannerSize.width, bannerSize.height)];
    } else {
        [_bannerView setFrame:CGRectMake(0, self.view.bounds.size.height-bannerSize.height, bannerSize.width, bannerSize.height)];
    }
    [_bannerView layoutIfNeeded];
}

-(void) setBannerPortrait {
    DDLogInfo(@"Banner to Portrait");
    _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    CGSize bannerSize = [ADBannerView sizeFromBannerContentSizeIdentifier:ADBannerContentSizeIdentifierPortrait];
    if (_bannerView.bannerLoaded == 0) {
        [_bannerView setFrame:CGRectMake(0, self.view.bounds.size.height, bannerSize.width, bannerSize.height)];
    } else {
        [_bannerView setFrame:CGRectMake(0, self.view.bounds.size.height-bannerSize.height, bannerSize.width, bannerSize.height)];
    }
    [_bannerView layoutIfNeeded];
}



- (void)hideBanner:(UIView *)banner{
    if (banner && ![banner isHidden]) {
        [UIView beginAnimations:@"bannerOff" context:NULL];
        banner.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
        [UIView commitAnimations];
        banner.hidden = YES;
    }
}

- (void)showBanner:(UIView *)banner{
    DDLogInfo((@"Show banner"));
    if (banner && [banner isHidden]) {
        [UIView beginAnimations:@"bannerOn" context:NULL];
        banner.frame = CGRectOffset(banner.frame, 0, -banner.frame.size.height);
        [UIView commitAnimations];
        banner.hidden = NO;
    }
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner{

    [self showBanner:banner];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
    [self hideBanner:_bannerView];
}
@end
