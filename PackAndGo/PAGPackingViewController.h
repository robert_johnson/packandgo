//
//  PAGPackingViewController.h
//  PackAndGo
//
//  Created by robert johnson on 3/19/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAGDbOperations;
@class PAGPackingItem;


@interface PAGPackingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

-(void) packedChanged:(BOOL)value onItem:(PAGPackingItem *)item;
-(void) shopForChanged:(BOOL)value onItem:(PAGPackingItem *)item;

@property (strong, nonatomic) PAGDbOperations *db;
@property (strong, nonatomic) NSNumber *tripId;
- (IBAction)doneButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *packingTableView;

@end
