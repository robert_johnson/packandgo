//
//  PAGTripDetails.h
//  PackAndGo
//
//  Created by robert johnson on 2/21/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PAGPackingListTemplate;

@interface PAGTripDetails : NSObject


@property (strong, nonatomic) NSString *tripName;
@property (strong, nonatomic) NSDate *tripStartDate;
@property (strong, nonatomic) PAGPackingListTemplate *packingListTemplate;
@property (strong, nonatomic) NSMutableArray *bags;
@property (strong, nonatomic) NSMutableDictionary *items;
@property (strong, nonatomic) NSMutableArray *itemLists;








@end
