//
// Created by rob on 2/26/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "PAGPackingListTemplateListViewController.h"
#import "PAGDbOperations.h"
#import "PAGPackingListTemplate.h"
#import "PAGPackingListTemplateListCell.h"
#import "PAGPackingListTemplateListPickerCell.h"
#import "PAGItemList.h"
#import "PAGPackingListTemplateListSectionHeader.h"
#import "DDLog.h"


static const int ddLogLevel = LOG_LEVEL_OFF;

BOOL editing;

@interface PAGPackingListTemplateListViewController () {
    NSString *sectionHeaderName;
    int sectionHeaderHeight;
}

@end

@implementation PAGPackingListTemplateListViewController {
    CGRect originalFrame;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    PAGDbOperations *db = [PAGDbOperations alloc];
    [db initDb];
    [self setDb:db];
    //set up a date formatter
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    // get a local copy of the item DB  ... now we should use the local copy here, and set it back when we are done
    [self setPackingListTemplate:[[_db packingListTemplate] mutableCopy]];
    editing = NO;
    _editingSection = -1;

    // set up some ipad/iphone specific stuff
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        // iPad-specific interface here
        sectionHeaderName = [NSString stringWithFormat:@"PAGPackingListTemplateListSectionHeader_iPad"];
        sectionHeaderHeight = 115;
    }
    else
    {
        // iPhone and iPod touch interface here
        sectionHeaderName = [NSString stringWithFormat:@"PAGPackingListTemplateListSectionHeader_iPhone"];
        sectionHeaderHeight = 100;
    }
    //register for datat updates
    [self registerForKeyboardNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"dataChanged" object:nil];

}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void) refresh {
    if (_db) {
        [self setPackingListTemplate:[[_db packingListTemplate] mutableCopy]];
        [_tableView reloadData];
    }
    
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark buttons

- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    // getting out of here, set the contents of the master array
    [_db setPackingListTemplate:_packingListTemplate];
    [_db saveData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataChanged" object:nil];

}

- (IBAction)newButtonPressed:(id)sender {
    // create a new item list in the db
    PAGPackingListTemplate *newPackingListTemplate = [_db createNewPackingListTemplateWithName:@"New Packing List Template" withItemLists:nil withCarryOnBags:[NSNumber numberWithInt:0] withCheckInBags:[NSNumber numberWithInt:0]];
    [_packingListTemplate addObject:newPackingListTemplate];
    NSIndexSet *newSections = [[NSIndexSet alloc] initWithIndex:[_packingListTemplate count]-1];
    [_tableView insertSections:newSections withRowAnimation:UITableViewRowAnimationAutomatic];

}

#pragma mark -
#pragma mark TableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = (NSInteger ) [_packingListTemplate count];
    DDLogInfo(@"numberOfSectionsInTableView = %x",count);

    return count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    PAGPackingListTemplate * packingListTemplate = [_packingListTemplate objectAtIndex:section];
    NSArray *itemList = packingListTemplate.itemList;
    NSUInteger count = itemList.count;

    if ((editing)&&(section == _editingSection)) {
        return count +1;
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"PAGPackingListTemplateListCell";
    static NSString *NewCellIdentifier = @"PAGPackingListTemplateListPickerCell";

    PAGPackingListTemplate * packingListTemplate = [_packingListTemplate objectAtIndex:indexPath.section];
    NSArray *itemList = packingListTemplate.itemList;
    NSUInteger count = itemList.count;


    if ((editing)&&(_editingSection == indexPath.section) && (indexPath.row >= count)) {
        PAGPackingListTemplateListPickerCell *cell = [tableView dequeueReusableCellWithIdentifier:NewCellIdentifier ];
        cell.itemViewController = self;
        cell.section = indexPath.section;
        cell.itemListPicker.frame= CGRectMake(cell.itemListPicker.frame.origin.x
                , cell.itemListPicker.frame.origin.y, cell.itemListPicker.frame.size.width, 162);
        cell.showsReorderControl = NO;

        return cell;
    }   else {
        PAGPackingListTemplateListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
        PAGItemList *list = [itemList objectAtIndex:indexPath.row];
        cell.itemListName.text = list.name;
        cell.itemListTotalItems.text = [NSString stringWithFormat:@"%ld",(unsigned long)list.item.count];
        cell.itemViewController = self;
        cell.showsReorderControl = YES;
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    PAGPackingListTemplate * packingListTemplate = [_packingListTemplate objectAtIndex:indexPath.section];
    NSArray *itemList = packingListTemplate.itemList;
    NSUInteger numItemList = itemList.count;

    // check if this is a 'new cell' if so, make it bigger!
    if (editing && (indexPath.row >= numItemList)) {
        CGFloat height = 162;
        return height;
    } else{
        CGFloat height = 31;
        return height;
    }
}


#pragma mark -
#pragma mark Section Headers
- (PAGPackingListTemplateListSectionHeader *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PAGPackingListTemplate * packingListTemplate = [_packingListTemplate objectAtIndex:section];
    PAGPackingListTemplateListSectionHeader *headerView = [[NSBundle mainBundle] loadNibNamed:sectionHeaderName owner:self options:nil][0];
    headerView.sectionIndex = section;
    headerView.tableView = self;
    headerView.templateName.text = packingListTemplate.name;
    int totalItemCount = 0;
    for (int j = 0; j < packingListTemplate.itemList.count; j++) {
        PAGItemList *i = [packingListTemplate.itemList objectAtIndex:j];
        totalItemCount = totalItemCount +   i.item.count;
    }
    headerView.numberOfCheckInBags.text = [NSString stringWithFormat:@"%@",packingListTemplate.numberOfCheckInBags];
    headerView.numberOfCarryOnBags.text = [NSString stringWithFormat:@"%@",packingListTemplate.numberOfCarryOnBags];
    headerView.totalNumberOfItems.text = [NSString stringWithFormat:@"%d",totalItemCount];
    if (section == _editingSection) {
        [headerView setIsEditing:YES];
        [[headerView editButton] setTitle:@"Done" forState:UIControlStateNormal];
    }
    DDLogInfo(@"viewForHeaderInSection = %x",section);
    return headerView;
}

- (void)sectionDidEnterEditMode:(NSUInteger) sectionIndex {

    _editingSection = sectionIndex;
    editing = YES;

    PAGPackingListTemplate * packingListTemplate = [_packingListTemplate objectAtIndex:sectionIndex];
    NSArray *itemList = packingListTemplate.itemList;
    NSUInteger numItems = itemList.count;

    [self.tableView setEditing:editing animated:YES];
    NSMutableArray *paths = [[NSMutableArray alloc] init];
    NSIndexPath *path = [NSIndexPath indexPathForRow: numItems inSection:sectionIndex];
    [paths addObject:path];
    [self.tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];

}

-(BOOL) sectionCanEnterEditMode:(NSUInteger) sectionIndex {
    if (editing == NO) {
        return YES;
    }
    return NO;
}




- (void)sectionDidLeaveEditMode:(NSUInteger)sectionIndex {
    _editingSection = -1;
    editing = NO;

    PAGPackingListTemplate * packingListTemplate1 = [_packingListTemplate objectAtIndex:sectionIndex];
    NSArray *itemList = packingListTemplate1.itemList;
    NSUInteger numItems = itemList.count;

    [self.tableView setEditing:editing animated:YES];
    NSMutableArray *paths = [[NSMutableArray alloc] init];
    NSIndexPath *path = [NSIndexPath indexPathForRow: numItems inSection:sectionIndex];
    [paths addObject:path];
    [self.tableView deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];


}

- (void)sectionDidDelete:(NSUInteger)sectionIndex {

    //[_db deleteItemList:sectionIndex];
    [_packingListTemplate removeObjectAtIndex:sectionIndex];
    //[_packingListTemplate setObject:nil atIndexedSubscript:sectionIndex];
    NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:sectionIndex];
    [_tableView deleteSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
    DDLogInfo(@"sectionDidDelete = %x",sectionIndex);
    DDLogInfo(@"Section List");
    for (PAGPackingListTemplate *t in _packingListTemplate) {
        DDLogInfo(@"template %@",t.name);
    }
    DDLogInfo(@"tableview think number of sections = %x",[_tableView numberOfSections]);
    for ( int i=0 ; i < [_tableView numberOfSections] ; i++ ) {
        PAGPackingListTemplateListSectionHeader *h = (PAGPackingListTemplateListSectionHeader *)[_tableView headerViewForSection:(NSInteger)i];
        DDLogInfo(@"table section %x thinks it is index %x (%@)",i,h.sectionIndex,h);
    }
    //update the following section headers
    NSRange range = NSMakeRange(sectionIndex, [_tableView numberOfSections] - sectionIndex);
    sections = [[NSIndexSet alloc] initWithIndexesInRange:range];
    [_tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
    
    //[_tableView reloadData];

}


- (void)sectionDidChangeName:(NSUInteger)sectionIndex toName:(NSString *)name{

    //[_db changeItemListName:sectionIndex toName:name];
    [[_packingListTemplate objectAtIndex:sectionIndex] setName:name];
}

- (void)sectionDidChangeCheckInBags:(NSUInteger)sectionIndex toString:(NSString *)checkInBags {
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    [[_packingListTemplate objectAtIndex:sectionIndex] setNumberOfCheckInBags:[f numberFromString:checkInBags]];
}

- (void)sectionDidChangeCarryOnBags:(NSUInteger)sectionIndex toString:(NSString *)carryOnBags {
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [[_packingListTemplate objectAtIndex:sectionIndex] setNumberOfCarryOnBags:[f numberFromString:carryOnBags]];
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return sectionHeaderHeight;
}

#pragma mark -
#pragma mark rows
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
// Return NO if you do not want the specified item to be editable.
    if (editing && (_editingSection == indexPath.section)) return YES;
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{

    PAGPackingListTemplate * packingListTemplate1 = [_packingListTemplate objectAtIndex:indexPath.section];
    NSArray *itemList = packingListTemplate1.itemList;
    NSUInteger numItemLists = itemList.count;

    if (_editingSection == indexPath.section) {
        if ((indexPath.row >= numItemLists)) {
            return UITableViewCellEditingStyleInsert;

        }   else {
            return UITableViewCellEditingStyleDelete;

        }
    } else {
        return nil;
    }


}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    {

        PAGPackingListTemplate * template = [_packingListTemplate objectAtIndex:indexPath.section];
        NSMutableArray *itemList = template.itemList;

        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            //[_db removeItemWithItemIndex:indexPath.row FromItemListWithIndex:indexPath.section];
            [itemList removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        } else if (editingStyle == UITableViewCellEditingStyleInsert)
        {
            PAGPackingListTemplateListPickerCell *cell = (PAGPackingListTemplateListPickerCell *)[tableView cellForRowAtIndexPath:indexPath];
            NSInteger itemIndex = [cell.itemListPicker selectedRowInComponent:0];
            PAGItemList *itemList = [_db getItemListForIndex:itemIndex];
            //[_db addItem:item ToItemListWithIndex:indexPath.section];
            [template addItemList:itemList];
            //[itemList addObject:item];
            cell.editingAccessoryType = UITableViewCellEditingStyleDelete;
            NSMutableArray *paths = [[NSMutableArray alloc] init];
            NSIndexPath *path = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
            [paths addObject:path];
            [tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];

        }
    }

}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)io {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return YES;
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait(io)) {
            return YES;
        } else {
            return NO;
        }
    }
    
}

#pragma mark -
#pragma mark keyboard specific stuff

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification

{
    NSLog(@"keyboardWasShown");
    // check that the notification came from a table view, as that is all we are interested in shrinking
    //DDLogInfo(@"Notification came from object %@",[aNotification userInfo]
    //                                               );
    DDLogInfo(@"Notification while active field = %@",_activeIndexPath);
    if (_activeIndexPath != nil) {
        
        CGSize keyFrameSize = [[[aNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey]CGRectValue].size;
        if (keyFrameSize.width < keyFrameSize.height)
        {
            // NOTE: fixing iOS bug: http://stackoverflow.com/questions/9746417/keyboard-willshow-and-willhide-vs-rotation
            CGFloat height = keyFrameSize.height;
            keyFrameSize.height = keyFrameSize.width;
            keyFrameSize.width = height;
        }
        DDLogInfo(@"keyboard frame , w=%f, h=%f",keyFrameSize.width,keyFrameSize.height);
        [UIView animateWithDuration:.025
                         animations:^{
                             
                             CGRect frame = _tableView.frame;
                             DDLogInfo(@"table frame , w=%f, h=%f, x=%f, y=%f",frame.size.width,frame.size.height,frame.origin.x,frame.origin.y);
                             originalFrame = frame;
                             frame.size.height = 69 + frame.size.height  -  keyFrameSize.height;
                             DDLogInfo(@"new table frame , w=%f, h=%f, x=%f, y=%f",frame.size.width,frame.size.height,frame.origin.x,frame.origin.y);
                             _tableView.frame = frame;}
         ];
        
        
        DDLogInfo(@"scrolling...");
        [self setTableResized:YES];
        [_tableView scrollToRowAtIndexPath:_activeIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    } else if (_tableResized) {
        [self keyboardWillBeHidden:nil];
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    NSLog(@"keyboardWillBeHidden");
    DDLogInfo(@"Notification while active field = %@",_activeIndexPath);
    if (!CGRectIsNull(originalFrame)) {
        [UIView animateWithDuration:.025 animations:^{
            _tableView.frame = originalFrame;
            [_tableView scrollRectToVisible:originalFrame animated:YES];
            originalFrame = CGRectNull;
            [self setTableResized:NO];
        }
         ];
    }
    
}


-(void)dismissKeyboard{
    [self.view endEditing:YES];
}




@end