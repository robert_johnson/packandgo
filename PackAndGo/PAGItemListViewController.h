//
//  PAGItemListViewController.h
//  PackAndGo
//
//  Created by robert johnson on 2/13/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PAGDbOperations;
@class PAGItemList;

@interface PAGItemListViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
NSDateFormatter *_dateFormatter;
    NSInteger _editingSection;
}
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) PAGDbOperations *db;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *activeIndexPath;
@property (nonatomic) BOOL tableResized;



@property(nonatomic, strong) NSMutableArray *itemList;

- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)newButtonPressed:(id)sender;

- (void)sectionDidEnterEditMode:(NSUInteger)sectionIndex;
- (BOOL) sectionCanEnterEditMode:(NSUInteger) sectionIndex;



- (void)sectionDidDelete:(NSUInteger)sectionIndex;

- (void)sectionDidChangeName:(NSUInteger)sectionIndex toName:(NSString *)name;

- (void)sectionDidLeaveEditMode:(NSUInteger)sectionIndex;
@end
