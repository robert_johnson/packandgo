//
//  PAGItemViewController.h
//  PackAndGo
//
//  Created by robert johnson on 2/11/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PAGDbOperations;
@class PAGItemCell;

@interface PAGItemViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    NSDateFormatter *_dateFormatter;
}
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) PAGDbOperations *db;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSIndexPath *activeIndexPath;
@property(nonatomic) CGPoint originalOffset;
@property (nonatomic) BOOL tableResized;



@property(nonatomic, strong) NSMutableArray *item;

- (IBAction)EditButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)doneButtonPressed:(id)sender;

- (void)cellContentsChanged:(PAGItemCell *)cell;
@end
