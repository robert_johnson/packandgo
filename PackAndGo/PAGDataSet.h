//
//  PAGDataSet.h
//  PackAndGo
//
//  Created by robert johnson on 4/10/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAGDataSet : NSObject <NSCoding>


@property (strong, nonatomic) NSMutableArray *packingList;
@property (strong, nonatomic) NSMutableArray *packingListTemplate;
@property (strong, nonatomic) NSMutableArray *itemList;
@property (strong, nonatomic) NSMutableArray *item;
@property (strong, nonatomic) NSMutableArray *tripDetails;

@end
