//
//  PAGModelTests.h
//  PAGModelTests
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "PAGItem.h"
#import "PAGItemList.h"
#import "PAGPackingList.h"
#import "PAGPackingListTemplate.h"
#import "PAGDbOperations.h"

@interface PAGModelTests : SenTestCase

@end
