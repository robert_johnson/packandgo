//
//  PAGPackingListTemplateViewController.h
//  PackAndGo
//
//  Created by robert johnson on 2/6/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PAGDbOperations.h"
#import "PAGTemplateListCell.h"

@interface PAGPackingListTemplateViewController : UIViewController   <UITableViewDelegate,UITableViewDataSource> {
    NSDateFormatter *_dateFormatter;
}
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) PAGDbOperations *db;


- (IBAction)doneButtonPressed:(id)sender;

@end
