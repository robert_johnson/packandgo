//
//  PAGItemListCell.h
//  PackAndGo
//
//  Created by robert johnson on 2/13/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PAGItemListViewController;


@interface PAGItemListCell : UITableViewCell
{
    UITextField *activeField;
}

@property (weak, nonatomic) IBOutlet UITextField *title;
@property (weak, nonatomic) IBOutlet UISwitch *perDay;
@property (nonatomic, strong) PAGItemListViewController *itemViewController;
@property (nonatomic, strong) NSIndexPath *indexPath;




@end
