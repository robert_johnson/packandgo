//
//  PAGPackingListTemplateListPickerCell.h
//  PackAndGo
//
//  Created by robert johnson on 2/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PAGPackingListTemplateListViewController;

@interface PAGPackingListTemplateListPickerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIPickerView *itemListPicker;

@property (nonatomic, strong) PAGPackingListTemplateListViewController *itemViewController;
@property (nonatomic) NSUInteger section;

@end
