//
//  PAGPackingListCell.h
//  PackAndGo
//
//  Created by robert johnson on 2/5/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PAGTripListCell : UITableViewCell 

@property (weak, nonatomic) IBOutlet UILabel *tripName ;

@property (weak, nonatomic) IBOutlet UILabel *dateDisplayField;

@end
