//
//  PAGTripDetailsSectionHeader.h
//  PackAndGo
//
//  Created by robert johnson on 3/18/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAGTripDetailsViewController;

@interface PAGTripDetailsSectionHeader : UIView <UITextFieldDelegate,UIActionSheetDelegate>



- (IBAction)editButtonPressed:(id)sender;
- (IBAction)deleteButtonPressed:(id)sender;
- (IBAction)textFieldEdited:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UIButton *isHold;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
- (IBAction)IsHoldButtonPressed:(id)sender;

@property(nonatomic) BOOL isEditing;
@property(nonatomic) NSUInteger sectionIndex;
@property(nonatomic, strong) PAGTripDetailsViewController *tableView;



@end
