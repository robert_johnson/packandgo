//
//  PAGSectionHeader.h
//  PackAndGo
//
//  Created by robert johnson on 2/18/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PAGItemListViewController;

@interface PAGSectionHeader : UIView <UITextFieldDelegate,UIActionSheetDelegate>
- (IBAction)editButtonPressed:(id)sender;
- (IBAction)deleteButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (nonatomic)     BOOL isEditing;

- (IBAction)textFieldEdited:(id)sender;
@property(nonatomic) NSUInteger sectionIndex;
@property(nonatomic, strong) PAGItemListViewController *tableView;


@end
