//
//  PAGBag.m
//  PackAndGo
//
//  Created by robert johnson on 3/7/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGBag.h"

@implementation PAGBag


- (id)init
{
    self = [super init];
    if (self) {
        _name = @"New Bag";
        _isCarryOn = NO;
        _items = [[NSMutableArray alloc]init];
        _itemLists = [[NSMutableArray alloc]init];
    }
    return self;
}
#pragma mark -
#pragma mark data encode and decode functions
- (void) encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:_name forKey:@"_name"];
    [aCoder encodeBool:_isCarryOn forKey:@"_isCarryOn"];
    

    [aCoder encodeObject:_items forKey:@"_items"];
    [aCoder encodeObject:_itemLists forKey:@"_itemLists"];
    
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self=[super init]) {
        
        self.name = [aDecoder decodeObjectForKey:@"_name"];
        self.isCarryOn = [aDecoder decodeBoolForKey:@"_isCarryOn"];
        
        self.items = [aDecoder decodeObjectForKey:@"_items"];
        self.itemLists = [aDecoder decodeObjectForKey:@"_itemLists"];
        
    }
    return self;
}


@end
