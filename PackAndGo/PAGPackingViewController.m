//
//  PAGPackingViewController.m
//  PackAndGo
//
//  Created by robert johnson on 3/19/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGTripDetailsViewController.h"
#import "PAGPackingViewController.h"
#import "PAGTripDetails.h"
#import "PAGDbOperations.h"
#import "PAGItem.h"
#import "PAGBag.h"
#import "PAGItemList.h"
#import "DDLog.h"
#import "PAGPackingItem.h"
#import "PAGPackingCell.h"



@interface PAGPackingViewController () {
    NSMutableArray *tripDetails;
    PAGTripDetails *tripDetail;
    NSDateFormatter *dateFormatter;
    UIPopoverController *popup;
    BOOL editing;
    
    
}

@end
@implementation PAGPackingViewController
static const int ddLogLevel = LOG_LEVEL_OFF;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self refresh];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"dataChanged" object:_tripId];

    DDLogInfo(@"PAGPackingViewController : View did load : done ");
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void) refresh {    
    tripDetails = [[_db tripDetails] mutableCopy];
    DDLogInfo(@"PAGPackingViewController : _tripId = %@",_tripId);
    if (_tripId == nil) {
        _tripId = [(PAGTripDetailsViewController *)[self tabBarController].viewControllers[0] tripId];
        _db = [(PAGTripDetailsViewController *)[self tabBarController].viewControllers[0] db];
        if (_tripId == nil) {
            DDLogInfo(@"PAGPackingViewController : No trip id, cant pack this bag");
        } else {
            DDLogInfo(@"PAGShoppingViewController : _tripId = %@",_tripId);
        }
        /**
        DDLogInfo(@"getting trip details : %@",_tripId);
        tripDetails = [[_db tripDetails] mutableCopy];
        tripDetail = [tripDetails objectAtIndex:[_tripId unsignedIntegerValue]];
        DDLogInfo(@"got trip details : %@",_tripId);
         **/
        
    } else {
        tripDetail = [tripDetails objectAtIndex:[_tripId unsignedIntegerValue]];
        DDLogInfo(@"PAGPackingViewController : got trip details : %@",_tripId);
    }
    
    // now check if the 'item list ' has been populated. if so, then check it is up to date
    // do this by copying all the itemList items into a big packingList of items.
    
    for (PAGBag *bag in tripDetail.bags) {
        for (PAGItemList *itemList in [bag itemLists]) {
            for (PAGItem *item in [itemList item]) {
                DDLogInfo(@"PAGPackingViewController : copy item : %@ - %@", [(PAGItemList*)itemList name], item.name);
                NSString *packingItemIndex = [NSString stringWithFormat:@"%@/%@" ,[(PAGItemList*)itemList name], item.name ];
                if ([tripDetail.items objectForKey:packingItemIndex] == nil)  {
                    PAGPackingItem *pi = [[PAGPackingItem alloc] init];
                    pi.packed = NO;
                    pi.perDay = item.perDay;
                    pi.shopFor = NO;
                    pi.name = item.name;
                    pi.bag = bag;
                    pi.list = itemList;
                    
                    [[tripDetail items] setObject:pi forKey:packingItemIndex];
                    
                } else {
                    DDLogInfo(@"PAGPackingViewController : item already exists in packing list");
                }
                
                
            }
        }
        
    }
    [_packingTableView reloadData];
    
}


#pragma mark -
#pragma mark tableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DDLogVerbose(@"PAGPackingViewController : numberOfRowsInSection : %lx",(long)[[tripDetail items] count]);
    return [[tripDetail items] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PAGPackingCell";
    DDLogInfo(@"PAGPackingViewController : tableView is asking for item at index %@",  indexPath);
    
    
    PAGPackingItem *p = [[[[tripDetail items] objectEnumerator] allObjects] objectAtIndex:indexPath.row];

    
    PAGPackingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    [cell.shopFor setSelected:p.shopFor];
    [cell.packed setSelected:p.packed];
    cell.itemName.text = p.name;

    cell.bagName.text = p.bag.name;
    cell.listName.text = p.list.name;
    cell.item = p;
    cell.packingViewController = self;
    
    cell.showsReorderControl = YES;
    return cell;
    
    
}

#pragma mark -
#pragma mark handle cell changes
-(void)packedChanged:(BOOL)value onItem:(PAGPackingItem *)item {
    item.packed = value;
    
}
-(void)shopForChanged:(BOOL)value onItem:(PAGPackingItem *)item {
    item.shopFor = value;
    
}

#pragma mark done button

- (IBAction)doneButtonPressed:(id)sender {
    DDLogInfo(@"PAGPackingViewController : done button pressed");
    [_db setTripDetails:tripDetails];
    [_db saveData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataChanged" object:_tripId];

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark handle interface

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)io {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return YES;
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait(io)) {
            DDLogInfo((@"YES"));
            return YES;
        } else {
            DDLogInfo((@"NO"));
            return NO;
        }
    }
    
}





- (void)viewDidUnload {
    [self setPackingTableView:nil];
    [super viewDidUnload];
}
@end
