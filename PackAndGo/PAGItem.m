//
//  PAGItem.m
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGItem.h"

@implementation PAGItem

#pragma mark -
#pragma mark data encode and decode functions
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeBool:_perDay forKey:@"perDay"];
    [aCoder encodeObject:_name forKey:@"name"];

}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self=[super init]) {

        self.perDay = [aDecoder decodeBoolForKey:@"perDay"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
    }
    return self;
}

@end
