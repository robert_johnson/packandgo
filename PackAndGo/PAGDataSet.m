//
//  PAGDataSet.m
//  PackAndGo
//
//  Created by robert johnson on 4/10/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGDataSet.h"

@implementation PAGDataSet

/**
 
 We need to store and encode the following data
 
 @property (strong, nonatomic) NSMutableArray *packingList;
 @property (strong, nonatomic) NSMutableArray *packingListTemplate;
 @property (strong, nonatomic) NSMutableArray *itemList;
 @property (strong, nonatomic) NSMutableArray *item;
 @property (strong, nonatomic) NSMutableArray *tripDetails;
 
 **/

- (id)init
{
    self = [super init];
    if (self) {
        self.packingList = [[NSMutableArray alloc] init];
        self.packingListTemplate = [[NSMutableArray alloc] init];
        self.item = [[NSMutableArray alloc] init];
        self.itemList = [[NSMutableArray alloc] init];
        self.tripDetails = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark -
#pragma mark data encode and decode functions
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_packingList forKey:@"_packingList"];
    [aCoder encodeObject:_packingListTemplate forKey:@"_packingListTemplate"];
    [aCoder encodeObject:_itemList forKey:@"_itemList"];
    [aCoder encodeObject:_item forKey:@"_item"];
    [aCoder encodeObject:_tripDetails forKey:@"_tripDetails"];
    
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self=[super init]) {
        
        self.packingList = [aDecoder decodeObjectForKey:@"_packingList"];
        self.packingListTemplate = [aDecoder decodeObjectForKey:@"_packingListTemplate"];
        self.itemList = [aDecoder decodeObjectForKey:@"_itemList"];
        self.item = [aDecoder decodeObjectForKey:@"_item"];
        self.tripDetails = [aDecoder decodeObjectForKey:@"_tripDetails"];
    }
    return self;
}

@end
