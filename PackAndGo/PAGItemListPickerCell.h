//
//  PAGItemListPickerCell.h
//  PackAndGo
//
//  Created by robert johnson on 2/14/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PAGItemListViewController;

@interface PAGItemListPickerCell : UITableViewCell  <UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, strong) PAGItemListViewController *itemViewController;
@property (nonatomic) NSUInteger section;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (nonatomic, strong) NSIndexPath *indexPath;


@end
