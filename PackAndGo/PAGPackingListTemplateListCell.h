//
//  PAGPackingListTemplateListCell.h
//  PackAndGo
//
//  Created by robert johnson on 2/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PAGPackingListTemplateListViewController;

@interface PAGPackingListTemplateListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *itemListName;
@property (weak, nonatomic) IBOutlet UILabel *itemListTotalItems;

@property (nonatomic, strong) PAGPackingListTemplateListViewController *itemViewController;
@property (nonatomic) NSUInteger section;

@end
