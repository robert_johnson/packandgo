//
//  PAGItemListPickerCell.m
//  PackAndGo
//
//  Created by robert johnson on 2/14/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGItemListPickerCell.h"
#import "PAGItemListViewController.h"
#import "PAGDbOperations.h"
#import "PAGItem.h"

@implementation PAGItemListPickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark -
#pragma mark picker






- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger count = [_itemViewController.db countItem];
    return count;}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    PAGItem *item = [_itemViewController.db getItemForIndex:row];
    return item.name;
}


@end
