//
//  PAGPackingListTemplate.m
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGPackingListTemplate.h"
#import "PAGItemList.h"
#import "DDLog.h"

static const int ddLogLevel = LOG_LEVEL_OFF;

@implementation PAGPackingListTemplate


- (void)setItemList:(NSMutableArray *)itemList {
    _itemList = [itemList mutableCopy];
}



- (void)addItemList:(PAGItem *)itemList {
    if (_itemList == nil) {
        DDLogInfo(@"init list");
        _itemList = [NSMutableArray arrayWithObject:itemList];
    } else{
        [_itemList addObject:itemList];
    }

}

#pragma mark -
#pragma mark data encode and decode functions
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_image forKey:@"image"];
    [aCoder encodeObject:_itemList forKey:@"itemList"];
    [aCoder encodeObject:_numberOfCarryOnBags forKey:@"numberOfCarryOnBags"];
    [aCoder encodeObject:_numberOfCheckInBags forKey:@"numberOfCheckInBags"];

}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self=[super init]) {


        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.image = [aDecoder decodeObjectForKey:@"image"];
        self.itemList = [aDecoder decodeObjectForKey:@"itemList"];
        self.numberOfCarryOnBags = [aDecoder decodeObjectForKey:@"numberOfCarryOnBags"];
        self.numberOfCheckInBags = [aDecoder decodeObjectForKey:@"numberOfCheckInBags"];



    }
    return self;
}

@end
