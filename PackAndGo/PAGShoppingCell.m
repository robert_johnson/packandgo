//
//  PAGShoppingCell.m
//  PackAndGo
//
//  Created by robert johnson on 3/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGShoppingCell.h"
#import "PAGShoppingViewController.h"
#import "DDLog.h"
@implementation PAGShoppingCell


static const int ddLogLevel = LOG_LEVEL_OFF;



- (IBAction)shopForChanged:(id)sender {
    DDLogInfo(@"shop for Changed");
    UIButton *b = ((UIButton *) sender);
    [b setSelected:![b isSelected]];
    BOOL shopFor = b.isSelected;
    [_shoppingViewController shopForChanged:shopFor onItem:_item];
    
}


#pragma mark UI stuff

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        DDLogInfo(@"init shopping cell");
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
