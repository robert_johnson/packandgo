//
//  PAGDbOperations.h
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAGPackingListTemplate;
@class PAGItem;
@class PAGItemList;
@class PAGPackingList;
@class PAGTripDetails;

@interface PAGDbOperations : NSObject <NSXMLParserDelegate> {
    BOOL dbInitialized;
    BOOL dbForceReinitialization;

}


@property (strong, nonatomic) PAGDbOperations *sharedDb;

@property (strong, nonatomic) NSMutableArray *packingList;
@property (strong, nonatomic) NSMutableArray *packingListTemplate;
@property (strong, nonatomic) NSMutableArray *itemList;
@property (strong, nonatomic) NSMutableArray *item;
@property (strong, nonatomic) NSMutableArray *tripDetails;

- (void) initDb;
- (void) saveData;
- (BOOL) loadData;

- (PAGItem *)createNewItemWithName:(NSString *)name withOwner:(NSString *)owner;
- (int) countItem;
- (PAGItem*) getItemForIndex:(NSUInteger)indexPath;
- (void)deleteItemAtIndex:(NSIndexPath *)indexPath;
- (PAGItemList *)createNewItemListWithName:(NSString *)name containingItems:(NSArray *)items withOwner:(NSString *)owner;
- (PAGPackingListTemplate *)createNewPackingListTemplateWithName:(NSString *)name withItemLists:(NSArray *)lists withCarryOnBags:(NSNumber *)carryOnBags
    withCheckInBags:(NSNumber *)checkInBags ;



- (NSUInteger ) countPackingListTemplate;
- (PAGPackingListTemplate*) getPackingListTemplateForIndex:(NSIndexPath *)indexPath;
- (PAGPackingList *) createNewPackingListWithTripName:(NSString *)tripName fromTemplate:(PAGPackingListTemplate *)template withDate:(NSDate *)tripDate;
- (NSUInteger ) countPackingLists;

- (PAGPackingList*) getPackingListForIndex:(NSIndexPath *)indexPath;

- (NSUInteger )countItemList;

- (NSUInteger )countItemsInItemList:(NSInteger)section;

- (PAGItem *)getItemFromItemList:(NSInteger)itemListIndex withIndex:(NSInteger)itemIndex;

- (PAGItemList *)getItemListForIndex:(NSInteger)index;

- (void)removeItemWithItemIndex:(NSUInteger)itemIndex FromItemListWithIndex:(NSUInteger)itemListIndex;

- (void)addItem:(PAGItem *)item ToItemListWithIndex:(NSInteger)index;

- (void)changeItemListName:(NSUInteger)itemListIndex toName:(NSString *)name;

- (void)deleteItemList:(NSUInteger)itemListIndex;

- (PAGTripDetails *)createNewTripWithTripName:(NSString *)tripName withTripSTartDate:(NSDate *)tripStartDate;
@end
