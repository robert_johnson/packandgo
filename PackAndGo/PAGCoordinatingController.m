//
//  PAGCoordinatingController.m
//  PackAndGo
//
//  Created by robert johnson on 2/6/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGCoordinatingController.h"

@implementation PAGCoordinatingController

static PAGCoordinatingController *sharedCoordinator = nil;

- (void) initialize
{
    _packingListViewController = [[PAGViewController alloc] init];
    _activeViewController = _packingListViewController;
}

+ (PAGCoordinatingController *) sharedInstance {
    NSLog(@"CC shared instance")  ;
    @synchronized (self) {
        if (sharedCoordinator == nil) {

            NSLog(@"was nil, allocwith zone and init");
            sharedCoordinator = [[super allocWithZone:NULL] init];
            [sharedCoordinator initialize];
        }
    }
    
    
    NSLog(@"CC return %@",sharedCoordinator);
    return sharedCoordinator;
    
    
}

#pragma mark -
#pragma mark overridden for singleton

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedInstance] ;
}

- (id) copyWithZone:(NSZone *)zone {
    return self;
}



#pragma mark -
#pragma mark view transition code

- (IBAction)requestViewChangedByObject:(id)object {
    NSLog(@"object %@  (with tag %d) sent view change request",object,[object tag]);

    if ([object isKindOfClass:[UIBarButtonItem class]]) {
        switch ([(UIBarButtonItem *)object tag]) {
            case PAGButtonTemplates: {
                NSLog(@"Try to load template view");
                NSLog(@"_packingListViewController  = %@" ,_packingListViewController);
                PAGPackingListTemplateViewController *controller = [_packingListViewController.storyboard instantiateViewControllerWithIdentifier:@"packingListTemplateViewController"]  ;
                NSLog(@"_packingListViewController storyboard = %@" ,_packingListViewController.storyboard);
                NSLog(@"Try to load template view : %@" , controller);
                [_packingListViewController.navigationController pushViewController:controller animated:YES];

            }
        }


    }
}




@end
