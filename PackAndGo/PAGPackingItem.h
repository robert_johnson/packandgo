//
//  PAGPackingItem.h
//  PackAndGo
//
//  Created by robert johnson on 3/20/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAGBag;
@class PAGItemList;

@interface PAGPackingItem : NSObject <NSCoding>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) PAGBag *bag;
@property (strong, nonatomic) PAGItemList *list;
@property (strong, nonatomic) NSString *owner;
@property ( nonatomic) BOOL perDay;
@property ( nonatomic) BOOL packed;
@property ( nonatomic) BOOL shopFor;

@end
