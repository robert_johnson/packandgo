//
//  PAGItemCell.m
//  PackAndGo
//
//  Created by robert johnson on 2/11/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGItemCell.h"
#import "PAGItemViewController.h"
#import "DDLog.h"

static const int ddLogLevel = LOG_LEVEL_OFF;


@implementation PAGItemCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)parDaySwitchChanged:(id)sender {
    [_itemViewController cellContentsChanged:self];
}





- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _itemViewController.activeIndexPath = self.indexPath;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];

    _itemViewController.activeIndexPath = nil;
    [_itemViewController cellContentsChanged:self];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
