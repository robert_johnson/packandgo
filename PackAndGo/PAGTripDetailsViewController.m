//
//  PAGTripDetailsViewController.m
//  PackAndGo
//
//  Created by robert johnson on 3/4/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGTripDetailsViewController.h"
#import "PAGDbOperations.h"
#import "PAGBag.h"
#import "PAGTripDetails.h"
#import "PAGPackingListTemplate.h"
#import "DDLog.h"
#import "PAGTripDetailsItemListCell.h"
#import "PAGItemList.h"
#import "PAGTripDetailsSectionHeader.h"

#import "PAGShoppingViewController.h"

@interface PAGTripDetailsViewController () {
    NSMutableArray *tripDetails;
    PAGTripDetails *tripDetail;
    NSDateFormatter *dateFormatter;
    UIPopoverController *popup;
    BOOL editing;
    UIView *templateInputAccView;
    UIView *dateInputAccView;
    UIButton *templateBtnDone;
    UIButton *dateBtnDone;
    CGRect accViewFrame;
    CGRect accButtonFrame;
    
    NSString *sectionHeaderName;
    CGFloat sectionHeaderHeight;

    
    
}

@end

@implementation PAGTripDetailsViewController {
    CGRect originalFrame;

}
static const int ddLogLevel = LOG_LEVEL_OFF;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        // iPad-specific interface here --
        sectionHeaderName = [NSString stringWithFormat:@"PAGTripDetailsSectionHeader_iPad"];
        sectionHeaderHeight = 68;
        
    } else {
        sectionHeaderName = [NSString stringWithFormat:@"PAGTripDetailsSectionHeader_iPhone"];
        sectionHeaderHeight = 81;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //set up a date formatter
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [self refresh];
    originalFrame = CGRectNull;
    // set up some ipad/iphone specific stuff

    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        // iPad-specific interface here --
        // scrap that, i like the pop-up better
        //accViewFrame = CGRectMake(10.0, 0.0, 310.0, 40.0);
        //accButtonFrame = CGRectMake(688.0, 0.0f, 80.0f, 40.0f);
        sectionHeaderName = [NSString stringWithFormat:@"PAGTripDetailsSectionHeader_iPad"];
        sectionHeaderHeight = 68;
        
    } else {
        sectionHeaderName = [NSString stringWithFormat:@"PAGTripDetailsSectionHeader_iPhone"];
        sectionHeaderHeight = 81;
        
        accViewFrame = CGRectMake(10.0, 0.0, 310.0, 40.0);
        accButtonFrame = CGRectMake(240.0, 0.0f, 80.0f, 40.0f);
        
        // iPhone and iPod touch interface here
        [self createDateInputAccessoryView];
        UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        datePicker.datePickerMode = UIDatePickerModeDate;
        //[datePicker addTarget:self action:@selector(datePopupDoneButtonPressed:) forControlEvents:UIControlEventValueChanged];
        _tripStartDate.inputView = datePicker;
        _tripStartDate.inputAccessoryView = dateInputAccView;
        
        [self createTemplateInputAccessoryView];
        
        UIPickerView *templatePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        templatePicker.delegate = self;
        templatePicker.dataSource = self;
        [templatePicker setShowsSelectionIndicator:YES];
        _templateField.inputView = templatePicker;
        _templateField.inputAccessoryView = templateInputAccView;
    }
    [self registerForKeyboardNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"dataChanged" object:_tripId];
    [_bagTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void) refresh {
    
    tripDetails = [[_db tripDetails] mutableCopy];
    if (_tripId == nil) {
        //        create a new trip ;
        tripDetail = [[PAGTripDetails alloc]init];
        tripDetail.items = [[NSMutableDictionary alloc]init];
        [tripDetail setTripName:@"My New Trip"];
        [tripDetail setTripStartDate:[[NSDate alloc]init]];
        
        _tripName.text=tripDetail.tripName;
        _tripStartDate.text = [dateFormatter stringFromDate:tripDetail.tripStartDate];
        
        
    } else {
        tripDetail = [tripDetails objectAtIndex:[_tripId unsignedIntegerValue]];
        if ([tripDetail items]==nil) {
            tripDetail.items = [[NSMutableDictionary alloc] init];
        }
        _tripName.text=tripDetail.tripName;
        _tripStartDate.text = [dateFormatter stringFromDate:tripDetail.tripStartDate];
        _templateField.text = tripDetail.packingListTemplate.name;
    }
    
    [_bagTableView reloadData];
    
}

- (void)viewDidUnload {
    [self setTripName:nil];
    [self setTripStartDate:nil];
    [self setDatePopupDatePicker:nil];
    [self setTemplatePopupDoneButton:nil];
    [self setTemplateField:nil];
    [self setBagTableView:nil];
    [super viewDidUnload];
}


-(void)dismissKeyboard{
    [self.view endEditing:YES];
}

- (IBAction)backgroundTouched:(id)sender {
    [self dismissKeyboard];
}

- (IBAction)tripNameEdited:(id)sender {
    //[self dismissKeyboard];
    [(UITextView *)sender resignFirstResponder];
    NSString *text = [(UITextView *)sender text];
    [tripDetail setTripName:text];
}

-(IBAction)tripStartDateTouched:(id)sender {
    UIViewController *pickerViewController = [[NSBundle mainBundle] loadNibNamed:@"PAGTripDetailDatePickerPopup_iPad" owner:self options:nil][0];
    pickerViewController.view.frame = CGRectMake(0, 0, 389, 216);
    pickerViewController.contentSizeForViewInPopover = CGSizeMake( 389, 216);
    [[pickerViewController view] setClipsToBounds:YES];
    popup = [[UIPopoverController alloc]initWithContentViewController:pickerViewController];
    [popup presentPopoverFromRect:_tripStartDate.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
     
    _tripStartDate.delegate = self;
  /**
    DDLogInfo(@"Loading popup");
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    DDLogInfo(@"datePicker = %@",datePicker);
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker addTarget:self action:@selector(datePopupDoneButtonPressed:) forControlEvents:UIControlEventValueChanged];
    [datePicker setHidden:NO];
    _tripStartDate.inputView = datePicker;
    DDLogInfo(@"datePicker = %@",_tripStartDate.inputView);

    [datePicker becomeFirstResponder];
     **/
}

- (IBAction)templateTouched:(id)sender {
    UIViewController *pickerViewController = [[NSBundle mainBundle] loadNibNamed:@"PAGTripDetailTemplatePickerPopup_iPad" owner:self options:nil][0];
    pickerViewController.view.frame = CGRectMake(0, 0, 389, 216);
    pickerViewController.contentSizeForViewInPopover = CGSizeMake( 389  , 216);
    [[pickerViewController view] setClipsToBounds:YES];
    
    popup = [[UIPopoverController alloc]initWithContentViewController:pickerViewController];
    [popup presentPopoverFromRect:_templateField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    // for some reason the keybaord is showing up for this contrl, but not for the date contrl!
    // dismiss it!
    UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    _templateField.inputView = dummyView;
    /**
    UIPickerView *templatePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    DDLogInfo(@"templatePicker = %@",templatePicker);
    templatePicker.delegate = self;
    templatePicker.dataSource = self;
    [templatePicker setShowsSelectionIndicator:YES];
    _templateField.inputView = nil;

    [pickerViewController becomeFirstResponder];
     **/
}


- (IBAction)templatePopupDoneButtonPressed:(id)sender {
    PAGPackingListTemplate *t;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        // iPad-specific interface here
        t = [[_db packingListTemplate] objectAtIndex:[_templatePopupPicker selectedRowInComponent:0]];
    }
    else
    {
        // iPhone and iPod touch interface here
        UIPickerView *p = (UIPickerView *)[_templateField inputView];
        [_templateField resignFirstResponder];
        t = [[_db packingListTemplate] objectAtIndex:[p selectedRowInComponent:0]];
    }
    
    [tripDetail setPackingListTemplate:t];
    [popup dismissPopoverAnimated:YES];
    _templateField.text = t.name;
    [self loadPackingListFromTemplate];
    [self save];
}


- (IBAction)datePopupDoneButtonPressed:(id)sender {
    NSDate *selectedDate;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        // iPad-specific interface here
        selectedDate = [_datePopupDatePicker date];
    }
    else
    {
        // iPhone and iPod touch interface here
        selectedDate = ((UIDatePicker*)[_tripStartDate inputView]).date;
    }
    
    
    [_tripStartDate resignFirstResponder];
    [tripDetail setTripStartDate:selectedDate];
    [popup dismissPopoverAnimated:YES];
    _tripStartDate.text = [dateFormatter stringFromDate:tripDetail.tripStartDate];
}

- (IBAction)tripStartDateEdited:(id)sender {
    [self dismissKeyboard];
    NSString *text = [(UITextView *)sender text];

    NSDate *date = [dateFormatter dateFromString:text];
    [tripDetail setTripStartDate:date];

}


- (IBAction)doneButtonPressed:(id)sender {
    [self dismissKeyboard];
    if (_tripId == nil) {
        [tripDetails addObject:tripDetail];
    } else {
        [tripDetails replaceObjectAtIndex:[_tripId unsignedIntegerValue] withObject:tripDetail];
    }
    [_db setTripDetails:tripDetails];
    [_db saveData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataChanged" object:_tripId];

    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void) loadPackingListFromTemplate {
    // copy the item lists to a local store (note : copy, not a reference..
    [tripDetail setItemLists:[tripDetail.packingListTemplate.itemList mutableCopy]];
    // packing list template should contain a list of all the bags required.
    
    NSMutableArray *bags = [[tripDetail bags] mutableCopy];
    if (bags == nil) { bags = [[NSMutableArray alloc] init];}
    // count current bags
    int numCurrentBags = [bags count];
    int numCurrentCheckInBags = 0;
    int numCurrentCarryOnBags = 0;
    for (int b=0; b<numCurrentBags; b++) {
        if ([bags[b] isCarryOn]) {
            numCurrentCarryOnBags++ ;
        } else {
            numCurrentCheckInBags++ ;
        }
    }
    // counted!! now check if we have enough of each
    NSNumber *numBags = tripDetail.packingListTemplate.numberOfCarryOnBags;
    numBags = [NSNumber numberWithInt:numBags.intValue - numCurrentCarryOnBags];
    for (int b=0; b<[numBags integerValue]; b++) {
        PAGBag *bag = [[PAGBag alloc] init];
        bag.isCarryOn = YES;
        bag.itemLists = [[NSMutableArray alloc] init];
        [bags addObject:bag];
    }
    
    numBags = tripDetail.packingListTemplate.numberOfCheckInBags;
    numBags = [NSNumber numberWithInt:numBags.intValue - numCurrentCheckInBags];
    for (int b=0; b<[numBags integerValue]; b++) {
        PAGBag *bag = [[PAGBag alloc] init];
        bag.isCarryOn = NO;
        bag.itemLists = [[NSMutableArray alloc] init];
        [bags addObject:bag];
    }
    [tripDetail setBags:bags];
    // now for each bag we need to add some item lists.....
    // ahh - add em all to the first bag
    NSMutableArray *itemLists = [[tripDetail packingListTemplate] itemList];
    PAGBag *bag = [[tripDetail bags] objectAtIndex:0];
    [bag setItemLists:itemLists];
    [_bagTableView reloadData];
}


#pragma mark -
#pragma mark tableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // get packing list in bag x
    PAGBag *b = [[tripDetail bags] objectAtIndex:section];
    return [b.itemLists count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PAGTripDetailsItemListCell";
    //static NSString *NewCellIdentifier = @"PAGBagCellPicker";
   
    PAGBag *bag = [[tripDetail bags] objectAtIndex:indexPath.section];
    PAGItemList *itemList = [[bag itemLists] objectAtIndex:indexPath.row];
    int count = [[bag itemLists] count];

    if ((editing)&&(_editingSection == _editingSection) && (indexPath.row >= count)) {
        
    }   else {
        PAGTripDetailsItemListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
        //PAGItemList *itemList = [itemList objectAtIndex:indexPath.row];
        cell.itemListName.text = itemList.name;
        cell.showsReorderControl = YES;
        return cell;
    }
    
    return nil;
    
    
}

#pragma mark -
#pragma mark Section Headers
- (PAGTripDetailsSectionHeader *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PAGBag * bag = [tripDetail.bags objectAtIndex:section];
    PAGTripDetailsSectionHeader *headerView = [[NSBundle mainBundle] loadNibNamed:sectionHeaderName owner:self options:nil][0];
    headerView.sectionIndex = section;
    headerView.tableView = self;
    headerView.name.text = bag.name;
    // use Yes and No for the switch
    //((UILabel *)[[[[[[headerView subviews] lastObject] subviews] objectAtIndex:2] subviews] objectAtIndex:0]).text = @"Yes";
    //((UILabel *)[[[[[[headerView subviews] lastObject] subviews] objectAtIndex:2] subviews] objectAtIndex:1]).text = @"No";

    
//    [headerView.isHold setOn:!bag.isCarryOn];
    [headerView.isHold setSelected:!bag.isCarryOn];
    return headerView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    return sectionHeaderHeight;
}

- (void)sectionDidChangeName:(NSUInteger)sectionIndex toName:(NSString *)name{
    
    //[_db changeItemListName:sectionIndex toName:name];
    PAGBag * bag = [tripDetail.bags objectAtIndex:sectionIndex];
    [bag setName:name];
}
- (void)sectionDidEnterEditMode:(NSNumber *)sectionIndex {
    
    _editingSection = sectionIndex ;
    editing = YES;
    
    //
    [self.bagTableView setEditing:YES animated:YES];
}

- (void)sectionDidChangeIsHold:(NSUInteger)sectionIndex toValue:(BOOL)isHold{
    
    //[_db changeItemListName:sectionIndex toName:name];
    PAGBag * bag = [tripDetail.bags objectAtIndex:sectionIndex];
    [bag setIsCarryOn:!isHold ];
}




- (void)sectionDidLeaveEditMode:(NSNumber *)sectionIndex {
    _editingSection = [NSNumber numberWithInt:-1];
    editing = NO;
    [self.bagTableView setEditing:NO animated:YES];
    // better save the changes here!!
    [_db setTripDetails:tripDetails];
        
    
}

- (void)sectionDidDelete:(NSUInteger)sectionIndex {
    
    // have to delete the bag and its contents
    
    NSMutableArray *bags = tripDetail.bags;
    [bags removeObjectAtIndex:sectionIndex];
    // better save the changes here!!
    [_db setTripDetails:tripDetails];
    [_bagTableView reloadData];
    
}

- (IBAction)newBagButtonPressed:(id)sender {
    
    // add a new bag here
    NSMutableArray *bags = tripDetail.bags;
    if (bags == nil) {

        bags = [[NSMutableArray alloc]init];

    }
    PAGBag *newBag = [[PAGBag alloc]init];
    [bags addObject:newBag];
    tripDetail.bags = bags;
    // better save the changes here!!
    [_db setTripDetails:tripDetails];
    [_bagTableView reloadData];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [tripDetail.bags count];
}

#pragma mark -
#pragma mark Row Moves

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    PAGBag * bag = [tripDetail.bags objectAtIndex:indexPath.section];

    NSArray *itemList = bag.itemLists;
    NSUInteger count = itemList.count;
    
    
    if ((editing)&&(_editingSection == _editingSection) && (indexPath.row >= count)) {
        return NO;
    }
    return YES;
}


- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    // now we have to move it in reality
    PAGBag * sourceBag = [tripDetail.bags objectAtIndex:sourceIndexPath.section];
    PAGBag * destinationBag = [tripDetail.bags objectAtIndex:destinationIndexPath.section];
    NSMutableArray *sourceItemList = [sourceBag.itemLists mutableCopy];
    NSMutableArray *destinationItemList = [destinationBag.itemLists mutableCopy];
    if ([sourceBag isEqual:destinationBag]) {
        [sourceItemList exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
        sourceBag.itemLists = sourceItemList;
    } else {
        PAGItemList *tmp = [sourceItemList objectAtIndex:sourceIndexPath.row];
        [sourceItemList removeObjectAtIndex:sourceIndexPath.row];
        
        [destinationItemList insertObject:tmp atIndex:destinationIndexPath.row];
        sourceBag.itemLists = sourceItemList;
        destinationBag.itemLists = destinationItemList;
    }
    
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editing && ([_editingSection  integerValue]== indexPath.section)) return YES;
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PAGBag * bag = [tripDetail.bags objectAtIndex:indexPath.section];
    NSArray *itemLists = bag.itemLists;
    NSUInteger numItems = itemLists.count;
    
    if ([_editingSection integerValue] == indexPath.section) {
        if ((indexPath.row >= numItems)) {
            return UITableViewCellEditingStyleInsert;
            
        }   else {
            return UITableViewCellEditingStyleDelete;
            
        }
    } else {
        return nil;
    }
    
    
}



-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[_db packingListTemplate] count];
}


-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    PAGPackingListTemplate *p = [[_db packingListTemplate] objectAtIndex:row];
    return p.name;
}



- (IBAction)leftSwipe:(id)sender {
    DDLogVerbose(@"Left swipe detected");
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)io {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return YES;
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait(io)) {
            return YES;
        } else {
            return NO;
        }
    }
    
}

#pragma mark -
#pragma mark Tab bar controller delegate

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    DDLogInfo(@"Performing Segue");
    return YES;
}


-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    DDLogInfo(@"Did select view controller"); 
    // save object so we can get a trip ID
    [self save];
    
    // just send a notification to update data, with the  trip ID
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataChanged" object:_tripId];
}

- (void) save {
    
    // save object so we can get a trip ID
    if (_tripId == nil) {
        DDLogInfo(@"trip ID was nil");
        [tripDetails addObject:tripDetail];
        _tripId = [NSNumber numberWithInt:[tripDetails indexOfObject:tripDetail]];
        DDLogInfo(@"trip ID is now %@",_tripId);
    } else {
        [tripDetails replaceObjectAtIndex:[_tripId unsignedIntegerValue] withObject:tripDetail];
    }
    [_db setTripDetails:tripDetails];
    [_db saveData];
    
}

#pragma mark -
#pragma mark keybaord stuff

-(void)createTemplateInputAccessoryView{
    templateInputAccView = [[UIView alloc] initWithFrame:accViewFrame];
    [templateInputAccView setBackgroundColor:[UIColor colorWithRed:135.0f green:206.0f blue:235.0f alpha:0]];
    [templateInputAccView setAlpha: 0.8];
    
    templateBtnDone = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [templateBtnDone setFrame:accButtonFrame];
    [templateBtnDone setTitle:@"Done" forState:UIControlStateNormal];
    //[btnDone setBackgroundColor:[UIColor greenColor]];
    [templateBtnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [templateBtnDone addTarget:self action:@selector(templatePopupDoneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    // Now that our buttons are ready we just have to add them to our view.
    [templateInputAccView addSubview:templateBtnDone];
}

-(void)createDateInputAccessoryView{
    dateInputAccView = [[UIView alloc] initWithFrame:accViewFrame];
    [dateInputAccView setBackgroundColor:[UIColor colorWithRed:135.0f green:206.0f blue:235.0f alpha:0]];
    [dateInputAccView setAlpha: 0.8];
    
    dateBtnDone = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [dateBtnDone setFrame:accButtonFrame];
    [dateBtnDone setTitle:@"Done" forState:UIControlStateNormal];
    //[btnDone setBackgroundColor:[UIColor greenColor]];
    [dateBtnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [dateBtnDone addTarget:self action:@selector(datePopupDoneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    // Now that our buttons are ready we just have to add them to our view.
    [dateInputAccView addSubview:dateBtnDone];
}

#pragma mark -
#pragma mark keyboard specific stuff

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}



// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification

{
    NSLog(@"keyboardWasShown");
    // check that the notification came from a table view, as that is all we are interested in shrinking
    //DDLogInfo(@"Notification came from object %@",[aNotification userInfo]
    //                                               );
    DDLogInfo(@"Notification while active field = %@",_activeIndexPath);
    if (_activeIndexPath != nil) {
    
    CGSize keyFrameSize = [[[aNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey]CGRectValue].size;
        if (keyFrameSize.width < keyFrameSize.height)
        {
            // NOTE: fixing iOS bug: http://stackoverflow.com/questions/9746417/keyboard-willshow-and-willhide-vs-rotation
            CGFloat height = keyFrameSize.height;
            keyFrameSize.height = keyFrameSize.width;
            keyFrameSize.width = height;
        }
    DDLogInfo(@"keyboard frame , w=%f, h=%f",keyFrameSize.width,keyFrameSize.height);
    [UIView animateWithDuration:.025
                     animations:^{
                         
                         CGRect frame = _bagTableView.frame;
                         DDLogInfo(@"table frame , w=%f, h=%f, x=%f, y=%f",frame.size.width,frame.size.height,frame.origin.x,frame.origin.y);
                         originalFrame = frame;
                         frame.size.height = 69 + frame.size.height  -  keyFrameSize.height;
                         DDLogInfo(@"new table frame , w=%f, h=%f, x=%f, y=%f",frame.size.width,frame.size.height,frame.origin.x,frame.origin.y);
                         _bagTableView.frame = frame;}
     ];
    
    
    DDLogInfo(@"scrolling...");
    [self setTableResized:YES];
    [_bagTableView scrollToRowAtIndexPath:_activeIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    } else if (_tableResized) {
        [self keyboardWillBeHidden:nil];
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    NSLog(@"keyboardWillBeHidden");
    DDLogInfo(@"Notification while active field = %@",_activeIndexPath);
    if (!CGRectIsNull(originalFrame)) {
    [UIView animateWithDuration:.025 animations:^{
        _bagTableView.frame = originalFrame;
        [_bagTableView scrollRectToVisible:originalFrame animated:YES];
        originalFrame = CGRectNull;
        [self setTableResized:NO];
    }
     ];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    DDLogInfo(@"textFieldShouldReturn %@" ,textField.text);
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidEndEditing %@" ,textField.text);
    [textField resignFirstResponder];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidBeginEditing %@" ,textField.text);
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldShouldEndEditing %@" ,textField.text);
    [textField resignFirstResponder];
    return YES;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldShouldEndEditing %@" ,textField.text);
    if ([textField tag] == 1){
        return YES;
    }
    return NO;  // Hide both keyboard and blinking cursor.
}





@end
