//
//  PAGTripDetailsViewController.h
//  PackAndGo
//
//  Created by robert johnson on 3/4/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PAGDbOperations;

@interface PAGTripDetailsViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource,UIPickerViewDelegate,UITabBarControllerDelegate>

- (void)sectionDidChangeName:(NSUInteger)sectionIndex toName:(NSString *)name;
- (void)sectionDidEnterEditMode:(NSNumber *)sectionIndex ;
- (void)sectionDidLeaveEditMode:(NSNumber *)sectionIndex ;
- (void)sectionDidDelete:(NSUInteger)sectionIndex ;
- (void)sectionDidChangeIsHold:(NSUInteger)sectionIndex toValue:(BOOL)isHold;

@property (strong, nonatomic) NSIndexPath *activeIndexPath;
@property (nonatomic) BOOL tableResized;


- (IBAction)newBagButtonPressed:(id)sender;

@property (strong, nonatomic) NSNumber *tripId;
@property (strong, nonatomic) PAGDbOperations *db;
- (IBAction)backgroundTouched:(id)sender;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePopupDatePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *templatePopupPicker;

@property (weak, nonatomic) IBOutlet UITextField *tripName;
- (IBAction) tripNameEdited:(id)sender;

- (IBAction) tripStartDateEdited:(id)sender;
- (IBAction) tripStartDateTouched:(id)sender;
- (IBAction)templateTouched:(id)sender;

- (IBAction)datePopupDoneButtonPressed:(id)sender;
- (IBAction)templatePopupDoneButtonPressed:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *templatePopupDoneButton;

@property (weak, nonatomic) IBOutlet UITableView *bagTableView;


@property (weak, nonatomic) IBOutlet UITextField *tripStartDate;
@property (weak, nonatomic) IBOutlet UITextField *templateField;

- (IBAction)doneButtonPressed:(id)sender;

@property ( strong,nonatomic) NSNumber *editingSection;


@end
