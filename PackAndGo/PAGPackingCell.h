//
//  PAGPackingCell.h
//  PackAndGo
//
//  Created by robert johnson on 3/19/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAGPackingViewController;
@class PAGPackingItem;


@interface PAGPackingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *itemName;
@property (weak, nonatomic) IBOutlet UILabel *bagName;
@property (weak, nonatomic) IBOutlet UILabel *listName;
@property (weak, nonatomic) IBOutlet UIButton *shopFor;
@property (weak, nonatomic) IBOutlet UIButton *packed;
- (IBAction)packedChanged:(id)sender;
- (IBAction)shopForChanged:(id)sender;


@property (nonatomic, strong) PAGPackingViewController *packingViewController;
@property (nonatomic) PAGPackingItem *item;
@end
