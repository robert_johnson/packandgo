//
//  PAGShoppingCell.h
//  PackAndGo
//
//  Created by robert johnson on 3/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PAGShoppingViewController;
@class PAGPackingItem;

@interface PAGShoppingCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *itemName;
@property (weak, nonatomic) IBOutlet UIButton *shopFor;
- (IBAction)shopForChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *listName;
@property (weak, nonatomic) IBOutlet UILabel *bagName;


@property (nonatomic, strong) PAGShoppingViewController *shoppingViewController;
@property (nonatomic) PAGPackingItem *item;

@end
