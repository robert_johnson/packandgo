//
//  PAGPackingList.m
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGPackingList.h"

@implementation PAGPackingList

#pragma mark -
#pragma mark data encode and decode functions
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_template forKey:@"template"];
    [aCoder encodeObject:_state forKey:@"state"];

}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self=[super init]) {


        self.template = [aDecoder decodeObjectForKey:@"template"];
        self.state = [aDecoder decodeObjectForKey:@"state"];

    }
    return self;
}


@end
