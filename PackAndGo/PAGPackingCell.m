//
//  PAGPackingCell.m
//  PackAndGo
//
//  Created by robert johnson on 3/19/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGPackingCell.h"
#import "PAGPackingViewController.h"
#import "DDLog.h"



@implementation PAGPackingCell
static const int ddLogLevel = LOG_LEVEL_INFO;


- (IBAction)packedChanged:(id)sender {
    DDLogInfo(@"packed Changed");
    UIButton *b = ((UIButton *) sender);
    [b setSelected:![b isSelected]];
    BOOL isPacked = b.isSelected;
    [_packingViewController packedChanged:isPacked  onItem:_item];
}

- (IBAction)shopForChanged:(id)sender {
    DDLogInfo(@"shop for Changed");
    UIButton *b = ((UIButton *) sender);
    [b setSelected:![b isSelected]];
    BOOL shopFor = b.isSelected;
    [_packingViewController shopForChanged:shopFor onItem:_item];
    
}
@end
