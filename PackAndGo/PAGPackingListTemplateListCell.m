//
//  PAGPackingListTemplateListCell.m
//  PackAndGo
//
//  Created by robert johnson on 2/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGPackingListTemplateListCell.h"

@implementation PAGPackingListTemplateListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
