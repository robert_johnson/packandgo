//
//  PAGCoordinatingController.h
//  PackAndGo
//
//  Created by robert johnson on 2/6/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAGViewController.h"
#import "PAGPackingListTemplateViewController.h"



@interface PAGCoordinatingController : NSObject

@property (nonatomic, readonly) UIViewController *activeViewController;
@property (nonatomic, readonly) PAGViewController *packingListViewController;
@property (nonatomic, readonly) PAGPackingListTemplateViewController *packingListTemplateViewController;

+(PAGCoordinatingController *) sharedInstance;

- (IBAction) requestViewChangedByObject:(id)object;


typedef enum {
    PAGButtonDone,
    PAGButtonNew,
    PAGButtonTrips,
    PAGButtonTemplates,
    PAGButtonItemLists,
    PAGButtonItems,
    PAGButtonConfiguration


}  ButtonTag   ;
@end
