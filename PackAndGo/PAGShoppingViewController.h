//
//  PAGShoppingViewController.h
//  PackAndGo
//
//  Created by robert johnson on 3/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PAGDbOperations;
@class PAGPackingItem;

@interface PAGShoppingViewController : UIViewController

-(void) shopForChanged:(BOOL)value onItem:(PAGPackingItem *)item;
-(void) createShoppingList ;
@property (weak, nonatomic) IBOutlet UITableView *shoppingTable;

@property (strong, nonatomic) PAGDbOperations *db;
@property (strong, nonatomic) NSNumber *tripId;
- (IBAction)doneButtonPressed:(id)sender;

@end
