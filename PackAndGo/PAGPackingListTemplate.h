//
//  PAGPackingListTemplate.h
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PAGItemList;

@interface PAGPackingListTemplate : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSMutableArray *itemList;
@property (strong, nonatomic) NSNumber *numberOfCarryOnBags;
@property (strong, nonatomic) NSNumber *numberOfCheckInBags;



- (void)addItemList:(PAGItemList *)itemList;

@end
