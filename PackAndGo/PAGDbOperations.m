//
//  PAGDbOperations.m
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGDbOperations.h"
#import "PAGPackingListTemplate.h"
#import "PAGItem.h"
#import "PAGItemList.h"
#import "PAGPackingList.h"
#import "PAGTripDetails.h"
#import "DDLog.h"
#import "PAGDataSet.h"

static const int ddLogLevel = LOG_LEVEL_INFO;


@implementation PAGDbOperations

static PAGDbOperations *sharedDb = nil;

+ (PAGDbOperations *) sharedInstance {
    DDLogInfo(@"***DB shared instance")  ;
    @synchronized (self) {
        if (sharedDb == nil) {
            DDLogInfo(@"***was nil, allocwith zone and init");
            sharedDb = [[super allocWithZone:NULL] init];
        }
    }


    DDLogInfo(@"***return %@",sharedDb);
    return sharedDb;


}


#pragma mark -
#pragma mark overridden for singleton

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedInstance] ;
}

- (id) copyWithZone:(NSZone *)zone {
    return self;
}


#pragma mark -
#pragma mark Database Operations

const NSString *defaultDbFileName = @"defaultDb";


- (void)initDb {
    DDLogInfo(@"Init database");
    dbForceReinitialization = NO;
    //load the database from a file if one exists, or create a new one
    if (dbForceReinitialization&&!dbInitialized) {
        //programatically initialise the database
        DDLogInfo(@"Force recreate of DB");
        [self createDb];
        dbInitialized = YES;
    } else if (!dbInitialized) {
        DDLogInfo(@"try to load DB from file");
        if (![self loadData]) {
            DDLogInfo(@"Failed, recreate");
            [self createDb];
        }
        dbInitialized = YES;
    }

    // - TODO


}
#pragma mark -
#pragma mark TripDetails

- (PAGTripDetails *)createNewTripWithTripName:(NSString *)tripName withTripSTartDate:(NSDate *)tripStartDate {

    PAGTripDetails *tripDetails = [[PAGTripDetails alloc]  init];
    [tripDetails setTripName:tripName];
    [tripDetails setTripStartDate:tripStartDate];

    [_tripDetails addObject:tripDetails];

    return tripDetails;


}

#pragma mark -
#pragma mark items


- (PAGItem *)createNewItemWithName:(NSString *)name withOwner:(NSString *)owner {
    PAGItem *item = [[PAGItem alloc] init];
    [item setName:name];
    [item setOwner:owner];

    [_item addObject:item];


    return item;
}

- (PAGItem *)getOrCreateNewItemWithName:(NSString *)name withOwner:(NSString *)owner {
    
    for (PAGItem *i in _item) {
        if ([i.name isEqualToString:name]) {
            return i;
        }
    }
    return [self createNewItemWithName:name withOwner:owner];

}

- (int) countItem{
    return _item.count;
}

- (PAGItem*) getItemForIndex:(NSUInteger )row{
    return [_item objectAtIndex:row];
}





- (void)deleteItemAtIndex:(NSIndexPath *)indexPath {

    [_item removeObjectAtIndex:indexPath.row];

}


#pragma mark -
#pragma mark Item list


- (void)deleteItemList:(NSUInteger)itemListIndex {
    [_itemList removeObjectAtIndex:itemListIndex];
}

- (void)changeItemListName:(NSUInteger)itemListIndex toName:(NSString *)name {
    PAGItemList *itemList = [_itemList objectAtIndex:itemListIndex];
    itemList.name=name;
}

- (void)addItem:(PAGItem *)item ToItemListWithIndex:(NSInteger)index {
    DDLogInfo(@"PAGDbOperations : adding item to item list");
    PAGItemList *itemList = [_itemList objectAtIndex:index];
    DDLogInfo(@"Item list = %@",itemList.item );

    [itemList addItem:item];
    DDLogInfo(@"Item list = %@",itemList.item );

}

- (void)removeItemWithItemIndex:(NSUInteger)itemIndex FromItemListWithIndex:(NSUInteger)itemListIndex {
    PAGItemList *itemList = [_itemList objectAtIndex:itemListIndex];
    [itemList.item removeObjectAtIndex:itemIndex];

}

- (id)createNewItemListWithName:(NSString *)name containingItems:(NSMutableArray *)items withOwner:(NSString *)owner {

    PAGItemList *itemList = [[PAGItemList alloc] init];
    [itemList setName:name ];
    [itemList setOwner:owner];
    [itemList setItem:items];

    [_itemList addObject:itemList];

    return itemList;

}
- (NSUInteger )countItemList {

    return _itemList.count;
}

- (NSUInteger )countItemsInItemList:(NSInteger)section {
    PAGItemList * itemList = [_itemList objectAtIndex:section];
    NSArray *items = itemList.item;
    int count = items.count;
    return count;
}
- (PAGItem *)getItemFromItemList:(NSInteger)itemListIndex withIndex:(NSInteger)itemIndex {
    PAGItemList * itemList = [_itemList objectAtIndex:itemListIndex];
    NSArray *items = itemList.item;
    PAGItem *item = [items objectAtIndex:itemIndex];
    return item;
}

- (PAGItemList *)getItemListForIndex:(NSInteger)index {

    PAGItemList * itemList = [_itemList objectAtIndex:index];

    return itemList;

}


#pragma mark -
#pragma mark Packing List Templates

- (PAGPackingListTemplate *)createNewPackingListTemplateWithName:(NSString *)name withItemLists:(NSMutableArray *)lists withCarryOnBags:(NSNumber *)carryOnBags withCheckInBags:(NSNumber *)checkInBags{
    PAGPackingListTemplate *packingListTemplate = [[PAGPackingListTemplate alloc]  init];
    [packingListTemplate setName:name];
    [packingListTemplate setItemList:lists];
    [packingListTemplate setNumberOfCarryOnBags:carryOnBags];
    [packingListTemplate setNumberOfCheckInBags:checkInBags];

    [_packingListTemplate addObject:packingListTemplate];

    return packingListTemplate;


}

- (NSUInteger) countPackingListTemplate{
    return _packingListTemplate.count;
}

- (PAGPackingListTemplate*) getPackingListTemplateForIndex:(NSIndexPath *)indexPath{
    return [_packingListTemplate objectAtIndex:indexPath.row];

    
}


- (PAGPackingList *)createNewPackingListWithTripName:(NSString *)tripName fromTemplate:(PAGPackingListTemplate *)template withDate:(NSDate *)tripDate {
    PAGPackingList *packingList = [[PAGPackingList alloc]  init];
    //[packingList setTripName:tripName];
    //[packingList setTripDate:tripDate];
    DDLogInfo(@"New packing list is : %@",packingList);
    [_packingList addObject:packingList];
    DDLogInfo(@"Packing list is : %@",_packingList);

    return packingList;

}

- (NSUInteger)countPackingLists {
    return _packingList.count;
}

- (PAGPackingList*) getPackingListForIndex:(NSIndexPath *)indexPath {

    return [_packingList objectAtIndex:indexPath.row];
}




#pragma mark -
#pragma mark Database save and load operations
- (BOOL) loadData {

    BOOL success = NO;

    // Create file manager
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory()
            stringByAppendingPathComponent:@"Documents"];
    
    NSString *dataFile = [documentsDirectory stringByAppendingPathComponent:@"/data.txt"];
    
    PAGDataSet  *dataSet;
    
    if ([fileMgr fileExistsAtPath:dataFile]) {
        DDLogInfo(@"%@ exists",dataFile);
        NSData *data = [fileMgr contentsAtPath:dataFile];
        dataSet = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        // copy data to correct places..
        self.packingList =  dataSet.packingList;
        self.packingListTemplate = dataSet.packingListTemplate;
        self.item = dataSet.item;
        self.itemList = dataSet.itemList;
        self.tripDetails = dataSet.tripDetails;
        
        
        success = YES;
    }



    return success;
}

- (void) saveData  {
    // Create file manager
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory()
            stringByAppendingPathComponent:@"Documents"];
    
    PAGDataSet *dataSet = [[PAGDataSet alloc]init];
    dataSet.packingList = _packingList;
    dataSet.packingListTemplate = _packingListTemplate;
    dataSet.item = _item;
    dataSet.itemList = _itemList;
    dataSet.tripDetails = _tripDetails;
    
    
    NSString *dataFile = [documentsDirectory stringByAppendingPathComponent:@"/data.txt"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dataSet];
    BOOL success = [fileMgr createFileAtPath:dataFile contents:data attributes:nil];
    DDLogInfo(@"saved items? (%d)", success);
    

}

#pragma mark -
#pragma mark Getters and Setters

- (NSMutableArray *)_packingList {
    if (_packingList == nil) {
        return [[NSMutableArray alloc]init];
    }
    return _packingList;
}

- (void)setPackingList:(NSMutableArray *)packingList {
    _packingList = [packingList mutableCopy];
}

- (NSMutableArray *)_packingListTemplate {
    if (_packingListTemplate == nil) {
        return [[NSMutableArray alloc]init];
    }
    return _packingListTemplate;
}

- (void)setPackingListTemplate:(NSMutableArray *)packingListTemplate {
    _packingListTemplate = [packingListTemplate mutableCopy];
}

- (NSMutableArray *)_itemList {
    DDLogInfo(@"getting Item List");
    if (_itemList == nil) {
        DDLogInfo(@"getting Item List : wasa null");
        return [[NSMutableArray alloc]init];
        
    }
    return _itemList;
}

- (void)setItemList:(NSMutableArray *)itemList {
    _itemList = [itemList mutableCopy];
}

- (NSMutableArray *)_item {
    if (_item == nil) {
        return [[NSMutableArray alloc]init];
    }
    return _item;
}

- (void)setItem:(NSMutableArray *)item {
    _item = [item mutableCopy];
}

- (NSMutableArray *)_tripDetails {
    if (_tripDetails == nil) {
        return [[NSMutableArray alloc]init];
    }
    return _tripDetails;
}

- (void)setTripDetails:(NSMutableArray *)tripDetails {
    _tripDetails = [tripDetails mutableCopy];
}

#pragma mark -
#pragma mark Create Default DB

- (void) createDb {
    
    DDLogInfo(@"Creating a fresh database");
    
    PAGDbOperations *db = self ;
    _item = [[NSMutableArray alloc]init];
    _itemList = [[NSMutableArray alloc]init];
    _packingList = [[NSMutableArray alloc]init];
    _packingListTemplate = [[NSMutableArray alloc]init];
    _tripDetails = [[NSMutableArray alloc] init];
    
    DDLogInfo(@"Creating a fresh database, db = %@", db);

    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [[NSBundle mainBundle] bundlePath];
    NSString *dataFile = [documentsDirectory stringByAppendingPathComponent:@"/PAGDefaultData.xml"];
    
    DDLogInfo(@"Data file = %@",dataFile);
    
    if ([fileMgr fileExistsAtPath:dataFile]) {
        DDLogInfo(@"%@ default data exists",dataFile);
        NSData *xmlData = [fileMgr contentsAtPath:dataFile];
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
        [xmlParser setDelegate:self];
        
        BOOL success = [xmlParser parse];
        if (success) {
            DDLogInfo(@"Loaded defaults");
        } else {
            DDLogInfo(@"Failed to load defaults");            
        }
        
        
    }

    
    
}

NSString *currentType;
NSString *currentItemListName;
NSString *currentPackingListTemplateName;
NSMutableArray *currentItems;
NSNumber *currentCarryOnBags;
NSNumber *currentCheckInBags;



-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    DDLogInfo(@"didStartElement %@",elementName);
    if ([elementName isEqualToString:@"PackAndGo"]) {
        //looks like we just started
    } else if ([elementName isEqualToString:@"ItemList"]) {
        if ([currentType isEqualToString:@"PackingListTemplate"]) {
            DDLogInfo(@"Add to packing list template");
            PAGItemList *i;
            for (PAGItemList *l in _itemList) {
                if ([l.name isEqualToString:[attributeDict objectForKey:@"name"]]) {
                    i = l;
                }
            }
            if (i) [currentItems addObject:i];
        } else {
            currentType = elementName;
            currentItemListName = [attributeDict objectForKey:@"name"];
            currentItems = [[NSMutableArray alloc]init];
        }
    } else if ([elementName isEqualToString:@"Item"]) {
        currentType = elementName;
    } else if ([elementName isEqualToString:@"PackingListTemplate"]) {
        currentType = elementName;
        currentPackingListTemplateName = [attributeDict objectForKey:@"name"];
        currentItems = [[NSMutableArray alloc]init];
        currentCarryOnBags = [attributeDict objectForKey:@"carryOnBags"];
        currentCheckInBags = [attributeDict objectForKey:@"checkInBags"];
        
    }
    
}
-(void)parser:(NSXMLParser *)parser foundAttributeDeclarationWithName:(NSString *)attributeName forElement:(NSString *)elementName type:(NSString *)type defaultValue:(NSString *)defaultValue {
    DDLogInfo(@"foundAttributeDeclarationWithName %@",attributeName);
    
}
-(void)parser:(NSXMLParser *)parser foundElementDeclarationWithName:(NSString *)elementName model:(NSString *)model {
    DDLogInfo(@"foundElementDeclarationWithName %@",elementName);
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    DDLogInfo(@"foundCharacters %@",string);
    if ([currentType isEqualToString:@"Item"]) {
        // must've found an item name.. create item (if it does not exist) and add to current list
        PAGItem *newItem = [self getOrCreateNewItemWithName:string withOwner:@"rob"];
        [currentItems addObject:newItem];
    }
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    DDLogInfo(@"didEndElement %@",elementName);
    
    if ([elementName isEqualToString:@"ItemList"]) {
        if ([currentType isEqualToString:@"PackingListTemplate"]) {
            //do nowt, we are addding to packing list template
        } else {
            [self createNewItemListWithName:currentItemListName containingItems:currentItems withOwner:@"robert"];
            currentType = nil;
        }

    } else if ([elementName isEqualToString:@"PackingListTemplate"]) {
        [self createNewPackingListTemplateWithName:currentPackingListTemplateName withItemLists:currentItems withCarryOnBags:currentCarryOnBags withCheckInBags:currentCheckInBags];
    
        DDLogInfo(@"Creating packinglist template");
        currentType = nil;
    } else if ([elementName isEqualToString:@"Item"]) {
        
        currentType = nil;
    }
    
    
}


@end