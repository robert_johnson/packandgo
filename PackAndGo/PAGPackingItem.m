//
//  PAGPackingItem.m
//  PackAndGo
//
//  Created by robert johnson on 3/20/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGPackingItem.h"

@implementation PAGPackingItem

- (id)init
{
    self = [super init];
    if (self) {
        _name = @"";
        _bag = nil;
        _list = nil;
        _owner = @"Rob";
        _perDay = NO;
        _packed = NO;
        _shopFor = NO;
    }
    return self;
}

#pragma mark -
#pragma mark data encode and decode functions
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_owner forKey:@"owner"];
    [aCoder encodeBool:_perDay forKey:@"perDay"];
    [aCoder encodeBool:_packed forKey:@"packed"];
    [aCoder encodeBool:_shopFor forKey:@"shopFor"];
    
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self=[super init]) {
        
        
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.owner = [aDecoder decodeObjectForKey:@"owner"];
        self.perDay = [aDecoder decodeBoolForKey:@"perDay"];
        self.packed = [aDecoder decodeBoolForKey:@"packed"];
        self.shopFor = [aDecoder decodeBoolForKey:@"shopFor"];
        
    }
    return self;
}


@end
