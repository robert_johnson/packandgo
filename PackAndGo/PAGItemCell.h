//
//  PAGItemCell.h
//  PackAndGo
//
//  Created by robert johnson on 2/11/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PAGItemViewController;

@interface PAGItemCell : UITableViewCell <UITextFieldDelegate> {
    UITextField *activeField;
}

@property (weak, nonatomic) IBOutlet UITextField *title;
@property (weak, nonatomic) IBOutlet UISwitch *perDay;
@property (nonatomic, strong) PAGItemViewController *itemViewController;
@property (nonatomic, strong) NSIndexPath *indexPath;


- (IBAction)parDaySwitchChanged:(id)sender;


@end
