//
//  PAGPackingList.h
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PAGPackingListTemplate;
@class PAGStateDb;


@interface PAGPackingList : NSObject

@property (strong, nonatomic) PAGPackingListTemplate *template;
@property (strong, nonatomic) PAGStateDb *state;


@end
