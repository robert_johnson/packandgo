//
//  PAGItemList.m
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGItemList.h"
#import "PAGItem.h"
#import "DDLog.h"
static const int ddLogLevel = LOG_LEVEL_OFF;


@implementation PAGItemList {
@private
    NSMutableArray *_item;
}

@synthesize item = _item;


- (void)setItem:(NSMutableArray *)item {
    _item = [item mutableCopy];
}

- (void)addItem:(PAGItem *)item {
    if (_item == nil) {
        DDLogInfo(@"init list");
        _item = [NSMutableArray arrayWithObject:item];
    } else{
        [_item addObject:item];
    }

}

#pragma mark -
#pragma mark data encode and decode functions
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_item forKey:@"item"];
    [aCoder encodeObject:_owner forKey:@"owner"];

}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self=[super init]) {


        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.item = [aDecoder decodeObjectForKey:@"item"];
        self.owner = [aDecoder decodeObjectForKey:@"owner"];

    }
    return self;
}


@end
