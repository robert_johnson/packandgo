//
//  PAGItemListViewController.m
//  PackAndGo
//
//  Created by robert johnson on 2/13/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGItemListViewController.h"
#import "PAGDbOperations.h"
#import "PAGItem.h"
#import "PAGItemListCell.h"
#import "PAGItemList.h"
#import "PAGItemListPickerCell.h"
#import "PAGSectionHeader.h"
#import "DDLog.h"

BOOL editing;
static const int ddLogLevel = LOG_LEVEL_OFF;


@interface PAGItemListViewController () {
    NSString *sectionHeaderName;
    int sectionHeaderHeight;
}

@end

@implementation PAGItemListViewController {
    CGRect originalFrame;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    PAGDbOperations *db = [PAGDbOperations alloc];
    [db initDb];
    [self setDb:db];
    //set up a date formatter
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    // get a local copy of the item DB  ... now we should use the local copy here, and set it back when we are done
    [self setItemList:[[_db itemList] mutableCopy]];
    //[self setItemList:_db.itemList];
    editing = NO;
    _editingSection = -1;
    
    // set up some ipad/iphone specific stuff
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        // iPad-specific interface here
        sectionHeaderName = [NSString stringWithFormat:@"PAGSectionHeader_iPad"];
        sectionHeaderHeight = 64;
    }
    else
    {
        // iPhone and iPod touch interface here
        sectionHeaderName = [NSString stringWithFormat:@"PAGSectionHeader_iPhone"];
        sectionHeaderHeight = 50;
    }
    [self registerForKeyboardNotifications ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"dataChanged" object:nil];
    
}
- (void)viewDidUnload {
    [super viewDidUnload];
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

-(void) refresh {
    if (_db) {
        [self setItemList:[[_db itemList] mutableCopy]];
        [_tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger *)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger *count = (NSInteger *) [_itemList count];

    
    return count;
}

- (NSInteger *)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    PAGItemList * itemList = [_itemList objectAtIndex:section];
    NSArray *items = itemList.item;
    NSUInteger count = items.count;
    if ((editing)&&(section == _editingSection)) {
        DDLogInfo(@"Number of rows in section %x = %x+1",section,count);
        return count +1;
    }
    DDLogInfo(@"Number of rows in section %x = %x",section,count);

    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PAGItemListCell";
    static NSString *NewCellIdentifier = @"PAGItemListPickerCell";

    PAGItemList * itemList = [_itemList objectAtIndex:indexPath.section];
    NSArray *items = itemList.item;
    NSUInteger count = items.count;


    if ((editing)&&(_editingSection == _editingSection) && (indexPath.row >= count)) {
        PAGItemListPickerCell *cell = [tableView dequeueReusableCellWithIdentifier:NewCellIdentifier ];
        cell.itemViewController = self;
        cell.section = indexPath.section;
        cell.picker.frame= CGRectMake(cell.picker.frame.origin.x
                , cell.picker.frame.origin.y, cell.picker.frame.size.width, 162);
        cell.showsReorderControl = NO;
        cell.indexPath = indexPath;
        return cell;
    }   else {
        PAGItemListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
        PAGItem *item = [items objectAtIndex:indexPath.row];
        cell.title.text = item.name;
        [cell.perDay setOn:item.perDay animated:YES];
        cell.itemViewController = self;
        cell.showsReorderControl = YES;
        cell.indexPath = indexPath;
        return cell;
    }

    
}

#pragma mark -
#pragma mark Section Headers
- (PAGSectionHeader *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    DDLogInfo(@"Custom Header Section Title being set or section %ld" , (long ) section);
    
    PAGItemList * itemList = [_itemList objectAtIndex:section];
    PAGSectionHeader *headerView = [[NSBundle mainBundle] loadNibNamed:sectionHeaderName owner:self options:nil][0];
    headerView.sectionIndex = section;
    headerView.tableView = self;
    headerView.name.text = itemList.name;
    if (section == _editingSection) {DDLogInfo(@"this is the editing section");
        [headerView setIsEditing:YES];
        [[headerView editButton] setTitle:@"Done" forState:UIControlStateNormal];
    }
    return headerView;
}

- (void)sectionDidEnterEditMode:(NSUInteger) sectionIndex {

    DDLogInfo(@"Section %x enters edit mode",sectionIndex);
    _editingSection = sectionIndex;
    editing = YES;

    PAGItemList * itemList = [_itemList objectAtIndex:sectionIndex];
    NSArray *items = itemList.item;
    NSUInteger numItems = items.count;

    [self.tableView setEditing:editing animated:YES];
    NSMutableArray *paths = [[NSMutableArray alloc] init];
    NSIndexPath *path = [NSIndexPath indexPathForRow: numItems inSection:sectionIndex];
    [paths addObject:path];
    [self.tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];

}

-(BOOL) sectionCanEnterEditMode:(NSUInteger) sectionIndex {
    if (editing == NO) {
        return YES;
    }
    return NO;
}



- (void)sectionDidLeaveEditMode:(NSUInteger)sectionIndex {
    DDLogInfo(@"Section left editing mode");
    _editingSection = -1;
    editing = NO;

    PAGItemList * itemList = [_itemList objectAtIndex:sectionIndex];
    NSArray *items = itemList.item;
    NSUInteger numItems = items.count;

    [self.tableView setEditing:editing animated:YES];
    NSMutableArray *paths = [[NSMutableArray alloc] init];
    NSIndexPath *path = [NSIndexPath indexPathForRow: numItems inSection:sectionIndex];
    [paths addObject:path];
    [self.tableView deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];


}

- (void)sectionDidDelete:(NSUInteger)sectionIndex {

    //[_db deleteItemList:sectionIndex];
    [_itemList removeObjectAtIndex:sectionIndex];
    NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:sectionIndex];
    [_tableView deleteSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];

}


- (void)sectionDidChangeName:(NSUInteger)sectionIndex toName:(NSString *)name{

    //[_db changeItemListName:sectionIndex toName:name];
    [[_itemList objectAtIndex:sectionIndex] setName:name];
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return sectionHeaderHeight;
}

#pragma mark -
#pragma mark Rows

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
// Return NO if you do not want the specified item to be editable.
    if (editing && (_editingSection == indexPath.section)){
     return YES;
    }
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    PAGItemList * itemList = [_itemList objectAtIndex:indexPath.section];
    NSArray *items = itemList.item;
    NSUInteger count = items.count;


    if ((editing)&&(_editingSection == _editingSection) && (indexPath.row >= count)) {
        return NO;
    }
    return YES;
}


- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    // now we have to move it in reality
    PAGItemList * itemList = [_itemList objectAtIndex:sourceIndexPath.section];
    NSMutableArray *items = [itemList.item mutableCopy];
    [items exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
    itemList.item = items;

}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    if (sourceIndexPath.section !=proposedDestinationIndexPath.section) {
        return sourceIndexPath;
    } else {
        return proposedDestinationIndexPath;
    }
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{

    PAGItemList * itemList = [_itemList objectAtIndex:indexPath.section];
    NSArray *items = itemList.item;
    NSUInteger numItems = items.count;

    if (_editingSection == indexPath.section) {
        if ((indexPath.row >= numItems)) {
            return UITableViewCellEditingStyleInsert;

        }   else {
            return UITableViewCellEditingStyleDelete;

        }
    } else {
        return nil;
    }


}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    PAGItemList * itemList = [_itemList objectAtIndex:indexPath.section];
    NSArray *items = itemList.item;
    NSUInteger numItems = items.count;

    // check if this is a 'new cell' if so, make it bigger!
    if (editing && (indexPath.row >= numItems)) {
        CGFloat height = 162;
        return height;
    } else{
        CGFloat height = 31;
        return height;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    {

        PAGItemList * itemList = [_itemList objectAtIndex:indexPath.section];
        NSMutableArray *items = itemList.item;

        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            [items removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        } else if (editingStyle == UITableViewCellEditingStyleInsert)
        {
            PAGItemListPickerCell *cell = (PAGItemListPickerCell *)[tableView cellForRowAtIndexPath:indexPath];
            NSInteger itemIndex = [cell.picker selectedRowInComponent:0];
            PAGItem *item = [_db getItemForIndex:itemIndex];
            [itemList addItem:item];
            cell.editingAccessoryType = UITableViewCellEditingStyleDelete;
            NSMutableArray *paths = [[NSMutableArray alloc] init];
            NSIndexPath *path = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
            [paths addObject:path];
            [tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];

        }
    }

}

#pragma mark -
#pragma mark buttons

- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    // getting out of here, set the contents of the master array
    [_db setItemList:_itemList];
    [_db saveData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataChanged" object:nil];

}

- (IBAction)newButtonPressed:(id)sender {
    // create a new item list in the db
    PAGItemList *newItemList = [_db createNewItemListWithName:@"new list" containingItems:nil withOwner:@"Rob"];
    [_itemList addObject:newItemList];

    NSIndexSet *newSections = [[NSIndexSet alloc] initWithIndex:[_itemList count]-1];
    [_tableView insertSections:newSections withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)io {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return YES;
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait(io)) {
            return YES;
        } else {
            return NO;
        }
    }
    
}

#pragma mark -
#pragma mark keyboard specific stuff

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification

{
    NSLog(@"keyboardWasShown");
    // check that the notification came from a table view, as that is all we are interested in shrinking
    //DDLogInfo(@"Notification came from object %@",[aNotification userInfo]
    //                                               );
    DDLogInfo(@"Notification while active field = %@",_activeIndexPath);
    if (_activeIndexPath != nil) {
        
        CGSize keyFrameSize = [[[aNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey]CGRectValue].size;
        if (keyFrameSize.width < keyFrameSize.height)
        {
            // NOTE: fixing iOS bug: http://stackoverflow.com/questions/9746417/keyboard-willshow-and-willhide-vs-rotation
            CGFloat height = keyFrameSize.height;
            keyFrameSize.height = keyFrameSize.width;
            keyFrameSize.width = height;
        }
        DDLogInfo(@"keyboard frame , w=%f, h=%f",keyFrameSize.width,keyFrameSize.height);
        [UIView animateWithDuration:.025
                         animations:^{
                             
                             CGRect frame = _tableView.frame;
                             DDLogInfo(@"table frame , w=%f, h=%f, x=%f, y=%f",frame.size.width,frame.size.height,frame.origin.x,frame.origin.y);
                             originalFrame = frame;
                             frame.size.height = 69 + frame.size.height  -  keyFrameSize.height;
                             DDLogInfo(@"new table frame , w=%f, h=%f, x=%f, y=%f",frame.size.width,frame.size.height,frame.origin.x,frame.origin.y);
                             _tableView.frame = frame;}
         ];
        
        
        DDLogInfo(@"scrolling...");
        [self setTableResized:YES];
        [_tableView scrollToRowAtIndexPath:_activeIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    } else if (_tableResized) {
        [self keyboardWillBeHidden:nil];
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    NSLog(@"keyboardWillBeHidden");
    DDLogInfo(@"Notification while active field = %@",_activeIndexPath);
    if (!CGRectIsNull(originalFrame)) {
        [UIView animateWithDuration:.025 animations:^{
            _tableView.frame = originalFrame;
            [_tableView scrollRectToVisible:originalFrame animated:YES];
            originalFrame = CGRectNull;
            [self setTableResized:NO];
        }
         ];
    }
    
}


-(void)dismissKeyboard{
    [self.view endEditing:YES];
}




@end
