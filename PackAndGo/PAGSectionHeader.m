//
//  PAGSectionHeader.m
//  PackAndGo
//
//  Created by robert johnson on 2/18/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGSectionHeader.h"
#import "PAGItemListViewController.h"
#import "DDLog.h"

static const int ddLogLevel = LOG_LEVEL_INFO;


@implementation PAGSectionHeader {

    NSInteger cancellationQueryYesButtonIndex;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)editButtonPressed:(id)sender {
    if (!_isEditing) {
        if ([_tableView sectionCanEnterEditMode:_sectionIndex]) {
            _isEditing = YES;
            DDLogInfo(@"Section %d did enter edit mode", _sectionIndex);
            [_tableView sectionDidEnterEditMode:_sectionIndex];
            [_editButton setTitle:@"Done" forState:UIControlStateNormal];
        }
    } else {
        _isEditing = NO;
        DDLogInfo(@"Section %d did leave edit mode", _sectionIndex);
        [_tableView sectionDidLeaveEditMode:_sectionIndex];
        [_editButton setTitle:@"Edit" forState:UIControlStateNormal];
    }


}

- (IBAction)deleteButtonPressed:(id)sender {
    // throw up a caution box
    UIActionSheet *cancellationQuery = [[UIActionSheet alloc]
            initWithTitle:@"Are You Sure?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
    cancellationQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    cancellationQueryYesButtonIndex = [cancellationQuery destructiveButtonIndex];
    [cancellationQuery showInView:self];
    //


}

- (IBAction)textFieldEdited:(id)sender {
    DDLogInfo(@"Section %d did change name", _sectionIndex);
    [_tableView sectionDidChangeName:_sectionIndex toName:self.name.text];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:_sectionIndex];
    [_tableView setActiveIndexPath:indexPath];
    DDLogInfo(@"textFieldDidBeginEditing");
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidEndEditing");
    [_tableView setActiveIndexPath:nil];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    DDLogInfo(@"textFieldShouldReturn");
    [textField resignFirstResponder];
    return YES;
}




-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == cancellationQueryYesButtonIndex) {
        DDLogInfo(@"Section %d did delete", _sectionIndex);
        [_tableView sectionDidDelete:_sectionIndex];
    }
}

@end
