//
//  PAGItemListCell.m
//  PackAndGo
//
//  Created by robert johnson on 2/13/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGItemListCell.h"
#import "PAGItemViewController.h"

@implementation PAGItemListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
