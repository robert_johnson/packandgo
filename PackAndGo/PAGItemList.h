//
//  PAGItemList.h
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAGItem;

@interface PAGItemList : NSObject

@property (strong, nonatomic) NSString *name;
@property (nonatomic, strong) NSMutableArray *item;
@property (strong, nonatomic) NSString *owner;


- (void)addItem:(PAGItem *)item;
@end
