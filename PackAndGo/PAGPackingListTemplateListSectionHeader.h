//
//  PAGPackingListTemplateListSectionHeader.h
//  PackAndGo
//
//  Created by robert johnson on 2/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PAGPackingListTemplateListViewController;

@interface PAGPackingListTemplateListSectionHeader : UIView <UITextFieldDelegate,UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UITextField *templateName;
@property (weak, nonatomic) IBOutlet UITextField *numberOfCheckInBags;

@property (weak, nonatomic) IBOutlet UITextField *numberOfCarryOnBags;
@property (weak, nonatomic) IBOutlet UITextField *totalNumberOfItems;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
- (IBAction)editButton:(id)sender;
- (IBAction)deleteButton:(id)sender;

@property(nonatomic) NSUInteger sectionIndex;
@property(nonatomic) BOOL isEditing;
@property(nonatomic, strong) PAGPackingListTemplateListViewController *tableView;

- (IBAction)packingListNameEdited:(id)sender;
- (IBAction)carryOnBagsEdited:(id)sender;
- (IBAction)checkInBagsEdited:(id)sender;

@end
