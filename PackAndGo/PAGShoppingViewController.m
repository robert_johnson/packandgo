//
//  PAGShoppingViewController.m
//  PackAndGo
//
//  Created by robert johnson on 3/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGShoppingViewController.h"
#import "PAGTripDetailsViewController.h"
#import "PAGTripDetails.h"
#import "PAGDbOperations.h"
#import "PAGItem.h"
#import "PAGBag.h"
#import "PAGItemList.h"
#import "DDLog.h"
#import "PAGPackingItem.h"
#import "PAGShoppingCell.h"

@interface PAGShoppingViewController () {
    NSMutableArray *tripDetails;
    PAGTripDetails *tripDetail;
    NSDateFormatter *dateFormatter;
    UIPopoverController *popup;
    BOOL editing;
    NSMutableArray *packingItems;
    
}
@end

@implementation PAGShoppingViewController


static const int ddLogLevel = LOG_LEVEL_OFF;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self refresh];
	// Do any additional setup after loading the view.

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"dataChanged" object:_tripId];
    
    DDLogInfo(@"PAGShoppingViewController : View did load : done ");
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) refresh {
    tripDetails = [[_db tripDetails] mutableCopy];
    DDLogInfo(@"PAGShoppingViewController : _tripId = %@",_tripId);
    if (_tripId == nil) {
        _tripId = [(PAGTripDetailsViewController *)[self tabBarController].viewControllers[0] tripId];
        _db = [(PAGTripDetailsViewController *)[self tabBarController].viewControllers[0] db];
        if (_tripId == nil) {
            DDLogInfo(@"PAGShoppingViewController : No trip id, cant shop for these items");
        } else {
            DDLogInfo(@"PAGShoppingViewController : _tripId = %@",_tripId);            
        }
        /**
        DDLogInfo(@"PAGShoppingViewController : getting trip details : %@",_tripId);
        tripDetails = [[_db tripDetails] mutableCopy];
        tripDetail = [tripDetails objectAtIndex:[_tripId unsignedIntegerValue]];
        DDLogInfo(@"PAGShoppingViewController : got trip details : %@",_tripId);
         **/
        
    } else {
        tripDetail = [tripDetails objectAtIndex:[_tripId unsignedIntegerValue]];
        DDLogInfo(@"PAGShoppingViewController : got trip details : %@",_tripId);
    }
    
    // now check if the 'item list ' has been populated. if so, then check it is up to date
    // do this by copying all the itemList items into a big packingList of items.
    
    for (PAGBag *bag in tripDetail.bags) {
        for (PAGItemList *itemList in [bag itemLists]) {
            for (PAGItem *item in [itemList item]) {
                DDLogInfo(@"PAGShoppingViewController : copy item : %@ - %@", [(PAGItemList*)itemList name], item.name);
                NSString *packingItemIndex = [NSString stringWithFormat:@"%@/%@" ,[(PAGItemList*)itemList name], item.name ];
                if ([tripDetail.items objectForKey:packingItemIndex] == nil)  {
                    PAGPackingItem *pi = [[PAGPackingItem alloc] init];
                    pi.packed = NO;
                    pi.perDay = item.perDay;
                    pi.shopFor = NO;
                    pi.name = item.name;
                    pi.bag = bag;
                    pi.list = itemList;
                    
                    [[tripDetail items] setObject:pi forKey:packingItemIndex];
                    
                } else {
                    DDLogInfo(@"PAGShoppingViewController : item already exists in packing list");
                }
                
                
            }
        }
        
    }
    DDLogInfo(@"PAGShoppingViewController : num items : %lx",(unsigned long)[[tripDetail items] count]);
    
    [self createShoppingList];
    [_shoppingTable reloadData];
    
}



#pragma mark -
#pragma mark tableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = [packingItems count];
    DDLogInfo(@"PAGShoppingViewController : number of items in table view = %x",count);
    return count;
}

-(void) createShoppingList {
    DDLogInfo(@"PAGShoppingViewController : create shopping list");
    packingItems = [[NSMutableArray alloc]init];
    for (PAGPackingItem *i in [[tripDetail items] objectEnumerator]) {
        
        if (i.shopFor) {
            DDLogInfo(@"PAGShoppingViewController : Adding item %@", i.name);
            [packingItems addObject:i];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PAGShoppingCell";
    DDLogInfo(@"PAGShoppingViewController : tableView is asking for item at index %@",  indexPath);
    
    
    //PAGPackingItem *p = [[[[tripDetail items] objectEnumerator] allObjects] objectAtIndex:indexPath.row];
    PAGPackingItem *p = [packingItems objectAtIndex:indexPath.row];
    
    PAGShoppingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    [cell.shopFor setSelected:p.shopFor];
    cell.itemName.text = p.name;
    cell.bagName.text = p.bag.name;
    cell.listName.text = p.list.name;
    cell.item = p;
    cell.shoppingViewController = self;
    
    cell.showsReorderControl = YES;
    return cell;
    
    
}

#pragma mark -
#pragma mark handle cell changes

-(void)shopForChanged:(BOOL)value onItem:(PAGPackingItem *)item {
    item.shopFor = value;
    
}

#pragma mark done button

- (IBAction)doneButtonPressed:(id)sender {
    DDLogInfo(@"PAGShoppingViewController : done button pressed");
    [_db setTripDetails:tripDetails];
    [_db saveData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataChanged" object:_tripId];

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark handle interface

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)io {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return YES;
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait(io)) {
            DDLogInfo((@"YES"));
            return YES;
        } else {
            DDLogInfo((@"NO"));
            return NO;
        }
    }
    
}

- (void)viewDidUnload {
    [self setShoppingTable:nil];
    [super viewDidUnload];
}
@end
