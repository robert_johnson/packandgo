//
//  PAGBag.h
//  PackAndGo
//
//  Created by robert johnson on 3/7/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAGBag : NSObject

@property (strong, nonatomic) NSString *name;
@property ( nonatomic) BOOL isCarryOn;
@property ( strong, nonatomic) NSMutableArray *items;
@property ( strong, nonatomic) NSMutableArray *itemLists;


@end
