//
//  PAGViewController.h
//  PackAndGo
//
//  Created by robert johnson on 1/31/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PAGDbOperations.h"
#import <iAd/iAd.h>

@interface PAGViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,ADBannerViewDelegate>

@property (strong, nonatomic) PAGDbOperations *db;

@property (weak, nonatomic) IBOutlet UITableView *PAGTripListTableView;


@end
