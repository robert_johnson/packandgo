//
//  PAGBaggageCell.h
//  PackAndGo
//
//  Created by robert johnson on 3/5/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PAGTripDetailsViewController;

@interface PAGTripDetailsItemListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *itemListName;


@end
