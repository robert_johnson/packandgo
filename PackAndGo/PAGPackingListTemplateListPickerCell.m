//
//  PAGPackingListTemplateListPickerCell.m
//  PackAndGo
//
//  Created by robert johnson on 2/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGPackingListTemplateListPickerCell.h"
#import "PAGPackingListTemplateListViewController.h"
#import "PAGDbOperations.h"
#import "PAGItemList.h"

@implementation PAGPackingListTemplateListPickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger count = [_itemViewController.db countItemList];
    return count;}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    PAGItemList *itemList = [_itemViewController.db getItemListForIndex:row];
    return itemList.name;
}

@end
