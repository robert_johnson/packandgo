//
//  PAGTripDetailsSectionHeader.m
//  PackAndGo
//
//  Created by robert johnson on 3/18/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "DDLog.h"
#import "PAGTripDetailsSectionHeader.h"
#import "PAGTripDetailsViewController.h"

@implementation PAGTripDetailsSectionHeader {
    NSInteger cancellationQueryYesButtonIndex;
}

static const int ddLogLevel = LOG_LEVEL_INFO;



- (IBAction)editButtonPressed:(id)sender {
    if (!_isEditing) {
        _isEditing = YES;
        DDLogInfo(@"Section %d did enter edit mode", _sectionIndex);
        [_tableView sectionDidEnterEditMode:[NSNumber numberWithInt:_sectionIndex]];
        [_editButton setTitle:@"Done" forState:UIControlStateNormal];
    } else {
        _isEditing = NO;
        DDLogInfo(@"Section %d did leave edit mode", _sectionIndex);
        [_tableView sectionDidLeaveEditMode:[NSNumber numberWithInt:_sectionIndex]];
        [_editButton setTitle:@"Edit" forState:UIControlStateNormal];
    }
    
    
}

- (IBAction)deleteButtonPressed:(id)sender {
    // throw up a caution box
    UIActionSheet *cancellationQuery = [[UIActionSheet alloc]
                                        initWithTitle:@"Are You Sure?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
    cancellationQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    cancellationQueryYesButtonIndex = [cancellationQuery destructiveButtonIndex];
    [cancellationQuery showInView:self];
    //
    
    
}

- (IBAction)textFieldEdited:(id)sender {
    DDLogInfo(@"Section %d did change name", _sectionIndex);
    [_tableView sectionDidChangeName:_sectionIndex toName:self.name.text];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:NSNotFound inSection:_sectionIndex];
    [_tableView setActiveIndexPath:indexPath];
    DDLogInfo(@"textFieldDidBeginEditing");
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidEndEditing");
    [_tableView setActiveIndexPath:nil];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == cancellationQueryYesButtonIndex) {
        DDLogInfo(@"Section %d did delete", _sectionIndex);
        [_tableView sectionDidDelete:_sectionIndex];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    DDLogInfo(@"textFieldShouldReturn");
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)IsHoldButtonPressed:(id)sender {
    UIButton *isHold = (UIButton *)sender;
    
    [isHold setSelected:![isHold isSelected]];
    [_tableView sectionDidChangeIsHold:_sectionIndex toValue:[isHold isSelected]];
}
@end
