//
//  PAGItemViewController.m
//  PackAndGo
//
//  Created by robert johnson on 2/11/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGItemViewController.h"
#import "PAGDbOperations.h"
#import "PAGItem.h"
#import "PAGItemCell.h"
#import "DDLog.h"

static const int ddLogLevel = LOG_LEVEL_OFF;


@interface PAGItemViewController ()   {
    BOOL editing;
}

@end

@implementation PAGItemViewController {
    CGRect originalFrame;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    PAGDbOperations *db = [PAGDbOperations alloc];
    [db initDb];
    [self setDb:db];
    // get a local copy of the item DB  ... now we should use the local copy here, and set it back when we are done
    [self setItem:[[_db item] mutableCopy]];
    //set up a date formatter
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    editing = NO;
    [self registerForKeyboardNotifications ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"dataChanged" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // your code
}

-(void) refresh {
    if (_db) {
        [self setItem:[[_db item] mutableCopy]];
        [_tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

#pragma mark -
#pragma mark templtaeview tbale dtatsource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = _item.count;
    if (editing) {
        return count +1;
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PAGItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PAGItemCell" ];
    if (editing && (indexPath.row >= _item.count)) {
        cell.title.text = @"New Item";
    }   else {
        PAGItem *item = [_item objectAtIndex:indexPath.row];
        cell.title.text = item.name;
        [cell.perDay setOn:item.perDay animated:YES];
    }
    cell.itemViewController = self;
    cell.indexPath = indexPath;
    return cell;
}


- (IBAction)EditButtonPressed:(UIBarButtonItem *)sender {
    editing = !editing;
    [self.tableView setEditing:editing animated:YES];
    if (editing) {
        NSArray *paths = [NSArray arrayWithObject:
                [NSIndexPath indexPathForRow:
                        _item.count
                                   inSection:0]];

        [self.tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];
        [sender setTitle:@"Done"];
    } else {
        NSArray *paths = [NSArray arrayWithObject:
                [NSIndexPath indexPathForRow:
                        _item.count
                                   inSection:0]];
        [self.tableView deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];
        [sender setTitle:@"Edit"];
        [self dismissKeyboard];
    }

}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ((indexPath.row >= _item.count)) {
        DDLogInfo(@"insert");
        return UITableViewCellEditingStyleInsert;
    }   else {
        DDLogInfo(@"delete");
        return UITableViewCellEditingStyleDelete;
        
    }
    
    
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *) indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        //[_db deleteItemAtIndex:indexPath];
        [_item removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

    } else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        PAGItemCell *cell = (PAGItemCell *)[tableView cellForRowAtIndexPath:indexPath];
        // need to call this one here so that we can create a new item
        PAGItem *newItem = [_db createNewItemWithName:cell.title.text withOwner:@"rob"];
        [_item addObject:newItem];
        
        //cell.editingAccessoryType = UITableViewCellEditingStyleDelete;
        NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:_item.count inSection:0]];
        [self.tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];
        [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];


    }
    [self sortItem];
}

#pragma mark -


- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    // getting out of here, set the contents of the master array
    [_db setItem:_item];
    [_db saveData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataChanged" object:nil];

}

- (void)cellContentsChanged:(PAGItemCell *)cell {
    //ignore changes if this is a new cell
    
    NSIndexPath * indexPath = [_tableView indexPathForCell:cell];
    if (indexPath.row >= (_item.count)) {
    }else {
        PAGItem *item = [_item objectAtIndex:indexPath.row];
        item.name = cell.title.text;
        item.perDay = cell.perDay.isOn;
        //[_db saveData];
        [self sortItem];
    }
}




- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

#pragma mark -
#pragma mark keyboard specific stuff

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];

}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification

{
    NSLog(@"keyboardWasShown");
    // check that the notification came from a table view, as that is all we are interested in shrinking
    //DDLogInfo(@"Notification came from object %@",[aNotification userInfo]
    //                                               );
    DDLogInfo(@"Notification while active field = %@",_activeIndexPath);
    if (_activeIndexPath != nil) {
        
        CGSize keyFrameSize = [[[aNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey]CGRectValue].size;
        if (keyFrameSize.width < keyFrameSize.height)
        {
            // NOTE: fixing iOS bug: http://stackoverflow.com/questions/9746417/keyboard-willshow-and-willhide-vs-rotation
            CGFloat height = keyFrameSize.height;
            keyFrameSize.height = keyFrameSize.width;
            keyFrameSize.width = height;
        }
        DDLogInfo(@"keyboard frame , w=%f, h=%f",keyFrameSize.width,keyFrameSize.height);
        [UIView animateWithDuration:.025
                         animations:^{
                             
                             CGRect frame = _tableView.frame;
                             DDLogInfo(@"table frame , w=%f, h=%f, x=%f, y=%f",frame.size.width,frame.size.height,frame.origin.x,frame.origin.y);
                             originalFrame = frame;
                             frame.size.height = 69 + frame.size.height  -  keyFrameSize.height;
                             DDLogInfo(@"new table frame , w=%f, h=%f, x=%f, y=%f",frame.size.width,frame.size.height,frame.origin.x,frame.origin.y);
                             _tableView.frame = frame;}
         ];
        
        
        DDLogInfo(@"scrolling...");
        [self setTableResized:YES];
        [_tableView scrollToRowAtIndexPath:_activeIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    } else if (_tableResized) {
        [self keyboardWillBeHidden:nil];
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    NSLog(@"keyboardWillBeHidden");
    DDLogInfo(@"Notification while active field = %@",_activeIndexPath);
    if (!CGRectIsNull(originalFrame)) {
        [UIView animateWithDuration:.025 animations:^{
            _tableView.frame = originalFrame;
            [_tableView scrollRectToVisible:originalFrame animated:YES];
            originalFrame = CGRectNull;
            [self setTableResized:NO];
        }
         ];
    }
    
}

-(void)dismissKeyboard{
    [self.view endEditing:YES];
}

-(void) sortItem{
    // now I must sort the array into alpha order..!!??

    NSArray *sorted;
    sorted = [_item sortedArrayUsingComparator:^(id obj1, id obj2) {
        NSString *objName = [(PAGItem *)obj1 name];
        NSString *obj2Name = [(PAGItem *)obj2 name];
        NSComparisonResult result = [objName compare:obj2Name options:NSNumericSearch | NSCaseInsensitiveSearch | NSWidthInsensitiveSearch | NSForcedOrderingSearch range:NSMakeRange(0, [objName length]) locale:[NSLocale currentLocale]];
        return result;
    }];
    if ([_item isEqualToArray:sorted]) {
    } else {
        // else make a copy and inform the table that it must reload its data
        _item = [sorted mutableCopy];
        [_tableView reloadData];
    }

}
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)io {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return YES;
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait(io)) {
            return YES;
        } else {
            return NO;
        }
    }
    
}




@end
