//
// Created by rob on 2/26/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class PAGDbOperations;


@interface PAGPackingListTemplateListViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    NSDateFormatter *_dateFormatter;
    NSInteger _editingSection;
}

@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) PAGDbOperations *db;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *packingListTemplate;

@property (strong, nonatomic) NSIndexPath *activeIndexPath;
@property (nonatomic) BOOL tableResized;



- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)newButtonPressed:(id)sender;

- (void)sectionDidEnterEditMode:(NSUInteger)sectionIndex;

-(BOOL) sectionCanEnterEditMode:(NSUInteger) sectionIndex;


- (void)sectionDidDelete:(NSUInteger)sectionIndex;

- (void)sectionDidChangeName:(NSUInteger)sectionIndex toName:(NSString *)name;

- (void)sectionDidLeaveEditMode:(NSUInteger)sectionIndex;


- (void)sectionDidChangeCheckInBags:(NSUInteger)sectionIndex toString:(NSString *)checkInBags;

- (void)sectionDidChangeCarryOnBags:(NSUInteger)sectionIndex toString:(NSString *)carryOnBags;
@end