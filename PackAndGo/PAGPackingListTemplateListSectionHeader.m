//
//  PAGPackingListTemplateListSectionHeader.m
//  PackAndGo
//
//  Created by robert johnson on 2/26/13.
//  Copyright (c) 2013 robert johnson. All rights reserved.
//

#import "PAGPackingListTemplateListSectionHeader.h"
#import "PAGPackingListTemplateListViewController.h"
#import "DDLog.h"

static const int ddLogLevel = LOG_LEVEL_OFF;


@implementation PAGPackingListTemplateListSectionHeader {
    
    NSInteger cancellationQueryYesButtonIndex;
}



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



- (IBAction)packingListNameEdited:(id)sender {
    DDLogInfo(@"PAGPackingListTemplateListSectionHeader : Section %d did change name", _sectionIndex);
    [_tableView sectionDidChangeName:_sectionIndex toName:self.templateName.text];
}

- (IBAction)carryOnBagsEdited:(id)sender {
    DDLogInfo(@"PAGPackingListTemplateListSectionHeader : Section %d did change number of carry on bags (to %@)", _sectionIndex,self.numberOfCarryOnBags.text);
    [_tableView sectionDidChangeCarryOnBags:_sectionIndex toString:self.numberOfCarryOnBags.text];
}

- (IBAction)checkInBagsEdited:(id)sender {
    DDLogInfo(@"PAGPackingListTemplateListSectionHeader : Section %d did change number of check in bags(to %@)", _sectionIndex,self.numberOfCheckInBags.text);
    [_tableView sectionDidChangeCheckInBags:_sectionIndex toString:self.numberOfCheckInBags.text];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:NSNotFound inSection:_sectionIndex];
    [_tableView setActiveIndexPath:indexPath];
    DDLogInfo(@"textFieldDidBeginEditing");
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    DDLogInfo(@"textFieldDidEndEditing");
    [_tableView setActiveIndexPath:nil];
}



- (IBAction)editButton:(id)sender {
    
        if (!_isEditing) {
            if ([_tableView sectionCanEnterEditMode:_sectionIndex]) {
                _isEditing = YES;
                DDLogInfo(@"PAGPackingListTemplateListSectionHeader : Section %d did enter edit mode", _sectionIndex);
                [_tableView sectionDidEnterEditMode:_sectionIndex];
                [_editButton setTitle:@"Done" forState:UIControlStateNormal];
            }
        } else {
            _isEditing = NO;
            DDLogInfo(@"PAGPackingListTemplateListSectionHeader : Section %d did leave edit mode", _sectionIndex);
            [_tableView sectionDidLeaveEditMode:_sectionIndex];
            [_editButton setTitle:@"Edit" forState:UIControlStateNormal];
        }

    
    
}

- (IBAction)deleteButton:(id)sender {
    // throw up a caution box
    UIActionSheet *cancellationQuery = [[UIActionSheet alloc]
                                        initWithTitle:@"Are You Sure?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
    cancellationQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    cancellationQueryYesButtonIndex = [cancellationQuery destructiveButtonIndex];
    [cancellationQuery showInView:self];
    //
    
    
}



-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == cancellationQueryYesButtonIndex) {
        DDLogInfo(@"PAGPackingListTemplateListSectionHeader : Section %d did delete", _sectionIndex);
        [_tableView sectionDidDelete:_sectionIndex];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    DDLogInfo(@"textFieldShouldReturn");
    [textField resignFirstResponder];
    return YES;
}

@end
